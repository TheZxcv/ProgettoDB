Scelte progettuali fatte:
- Una gara di un torneo ad eliminazione ha UN SOLO vincitore. whew

- Il cambiamento della quota di iscrizione per singoli Giocatori è stata espressa tramite
	il campo sconto_rincaro nelle relazione Iscritto_a che rappresenta la percentuale
	di sconto, nel caso sia negativa, o di rincaro, nel caso sia positiva.

- La classifica sarà implementata tramite una vista sulle tabelle Giocatore, Partecipa_a e Gara.

- Un Torneo Misto è l'insieme di diversi tornei, n tornei all'italiana che rappresentano i diversi
	gironi e un torneo ad eliminazione che rappresenta la fase finale.

- Nella tabella Gara è presente un campo opzionale Fase utilizzato nel caso di un torneo ad eliminazione
	per semplificare la distinzione della diverse fasi.

- *MAY BE REMOVED* Non sono permessi due tornei con lo stesso nome.

- *FIX ME* Le gare sono generate in sequenza partendo da una data,
	ad esempio se la prima gara è il 12/01/2015, la seconda sarà il 13/01/2015
	e così via.

- Punteggio e Risultato di una Gara: il risultato di una gara rappresenta come si è conclusa la gara,
ad esempio con che punteggio ogni Giocatore ha concluso la gara, invece il Punteggio rappresenta i punti
ottenuti da ogni Giocatore in base al Risultato della gara.

- La generazione delle gare nei tornei all'italiana con più di due partecipanti per gara è stata fatta
	generando n gare, dove n è il numero dei giocatori, composte da giocatori scelti casualmente però
	garantendo che ogni giocatore giochi almeno in una gara.

- *ADD MORE*

Entità Utente:
L'entità Utente identifica un utente generico iscritto alla piattaforma. Questo possiede uno username
e una password. Inoltre, possiede l'attributo booleano bannato che è vero nel caso in cui all'utente sia stato
negato l'accesso alla piattaforma.

L'entità Utente ha due entità figlie: Giocatore, che in più possiede i dati anagrafici e quelli riguardo alla
partecipazioni ai tornei; Amministratore, che amministra la piattaforma. Un amministratore può non essere un Giocatore.

L'entità Giocatore ha una entità figlia Organizzatore che contiene tutti i Giocatori che possono in più organizzare
tornei.


Entità Torneo:
L'entità Torneo identifica un generico torneo con i suoi attributi. Ha una associazione con se stesso che rappresenta
la possibilità di avere riedizioni di uno stesso torneo.

Vincoli: min_partecipanti > 1 e min_partecipanti <= max_partecipanti

L'entità Torneo possiede tre entità figlie: Italiana, Eliminazione e Misto.
Queste entità rappresentano tre possibili stutture di torneo differenti.
Nel caso del Torneo Misto si hanno due associazioni, una con l'entità Italiana per rappresentare l'associazione con i suoi gironi
e una con l'entità Eliminazione per rappresentare la sua ultima fase ad eliminazione.

L'attributo num_gironi di Torneo Misto è una ridondanza perché si potrebbe calcolare tramite la tabella Possiede_gironi, ma è
utilizzato per la generazione in differita dei gironi stessi.

Vincolo: un Torneo con struttura misto non possiede nessuna gara, ma un insieme di torneo, tanti tornei Italiana quanti sono i gironi
e un torneo ad Eliminazione finale.





SCHEMA RELAZIONE

Utente (>idUtente<, user, password)
Amministratore (>idUtente<, data_creazione)
Giocatore (>idUtente<, nome, cognome, organizzatore)
Torneo (>idTorneo<, *nome, *scadenza_iscr, quota_iscr, *num_gare, struttura, *min_partecipanti, *max_partecipanti, *idTorneo_precedente, idOrganizzatore, *premio)
Torneo_Misto (>idTorneo<, *idTorneo_Eliminazione, num_gironi)
Possiede_gironi (>idTorneo_Misto, idTorneo_allitaliana<, *Girone)
Gara (>idGara, idTorneo<, *data, *fase)
Iscritto_a (>idGiocatore, idTorneo<, approvata, sconto_rincaro)
Ha_vinto (>idGiocatore, idTorneo<)
Partecipa_a (>idGiocatore, idGara, idTorneo<, *punteggio, *risultato)
Commento (>idCommento<, idGiocatore, idGara, idTorneo, testo, data, idCommento_parent)
Utente_bannato (>idUtente<) // Potrebbe essere come attributo, noi abbiamo optato per la relazione

Si è scelto di ristrutturare la generalizzazione di Giocatore aggiungendo un attributo booleano organizzatore all'entità Giocatore. Invece, per l'entità Utente si è deciso di lasciare le entità figlie come entità 'deboli' collegate con una relazione a Utenti.

La generalizzazione di Torneo è stata ristrutturata aggiungendo l'attributo struttura che ne identifica il tipo, tranne nel caso di Torneo_Misto per cui è stata creata una entità 'debole'.

L'attributo opzionale bannato è stato tradotto come tabella in cui saranno inseriti gli ID degli utenti bannati.
