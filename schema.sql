CREATE SCHEMA IF NOT EXISTS db_progetto DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE db_progetto;

-- -----------------------------------------------------
-- Table db_progetto.Utente
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS db_progetto.Utente (
	idUtente BIGINT NOT NULL AUTO_INCREMENT,
	user VARCHAR(20) NOT NULL,
	password VARCHAR(256) NOT NULL COMMENT 'SHA-256 della password',
	PRIMARY KEY (idUtente),
	UNIQUE INDEX idUtente_UNIQUE (idUtente ASC),
	UNIQUE INDEX user_UNIQUE (user ASC)
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table db_progetto.Amministratore
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS db_progetto.Amministratore (
	idUtente BIGINT NOT NULL,
	data_creazione TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	INDEX fk_Amministratore_Utente_idx (idUtente ASC),
	PRIMARY KEY (idUtente),
	CONSTRAINT fk_Amministratore_Utente
		FOREIGN KEY (idUtente)
		REFERENCES db_progetto.Utente (idUtente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table db_progetto.Giocatore
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS db_progetto.Giocatore (
	idUtente BIGINT NOT NULL,
	nome VARCHAR(50) NOT NULL,
	cognome VARCHAR(50) NOT NULL,
	organizzatore TINYINT(1) NOT NULL DEFAULT 0,
	INDEX fk_Giocatore_Utente1_idx (idUtente ASC),
	PRIMARY KEY (idUtente),
	CONSTRAINT fk_Giocatore_Utente1
		FOREIGN KEY (idUtente)
		REFERENCES db_progetto.Utente (idUtente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table db_progetto.Torneo
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS db_progetto.Torneo (
	idTorneo BIGINT NOT NULL AUTO_INCREMENT,
	nome VARCHAR(45) NULL,
	scadenza_iscr DATE NULL,
	quota_iscr DECIMAL(5,2) NOT NULL,
	num_gare INT NULL COMMENT 'sarà calcolato automaticamente quando possibile',
	struttura INT NOT NULL COMMENT 'Creare un dominio apposta per questo tipo',
	min_partecipanti INT NOT NULL,
	max_partecipanti INT NOT NULL,
	idTorneo_precedente BIGINT NULL,
	idOrganizzatore BIGINT NOT NULL,
	premio VARCHAR(45) NULL,
	PRIMARY KEY (idTorneo),
	UNIQUE INDEX idTorneo_UNIQUE (idTorneo ASC),
	INDEX fk_Torneo_Torneo2_idx (idTorneo_precedente ASC),
	INDEX fk_Torneo_Giocatore1_idx (idOrganizzatore ASC),
	UNIQUE INDEX nome_UNIQUE (nome ASC),
	CONSTRAINT chk_Partecipanti 
		CHECK (min_partecipanti > 1 AND max_partecipanti >= min_partecipanti),
	CONSTRAINT chk_Quota
		CHECK (quota_iscr >= 0.0),
	CONSTRAINT fk_Torneo_Torneo2
		FOREIGN KEY (idTorneo_precedente)
		REFERENCES db_progetto.Torneo (idTorneo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT fk_Torneo_Giocatore1
		FOREIGN KEY (idOrganizzatore)
		REFERENCES db_progetto.Giocatore (idUtente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table db_progetto.Torneo_Misto
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS db_progetto.Torneo_Misto (
	idTorneo BIGINT NOT NULL,
	idTorneo_Eliminazione BIGINT NULL,
	num_gironi INT NOT NULL,
	PRIMARY KEY (idTorneo),
	INDEX fk_Torneo_Misto_Torneo2_idx (idTorneo_Eliminazione ASC),
	CONSTRAINT fk_Torneo_Misto_Torneo1
		FOREIGN KEY (idTorneo)
		REFERENCES db_progetto.Torneo (idTorneo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT fk_Torneo_Misto_Torneo2
		FOREIGN KEY (idTorneo_Eliminazione)
		REFERENCES db_progetto.Torneo (idTorneo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table db_progetto.Possiede_girone
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS db_progetto.Possiede_girone (
	idTorneo_Misto BIGINT NOT NULL,
	idTorneo_allitaliana BIGINT NOT NULL,
	Girone TINYINT NULL,
	PRIMARY KEY (idTorneo_Misto, idTorneo_allitaliana),
	INDEX fk_Torneo_Misto_has_Torneo_Torneo1_idx (idTorneo_allitaliana ASC),
	INDEX fk_Torneo_Misto_has_Torneo_Torneo_Misto1_idx (idTorneo_Misto ASC),
	CONSTRAINT fk_Torneo_Misto_has_Torneo_Torneo_Misto1
		FOREIGN KEY (idTorneo_Misto)
		REFERENCES db_progetto.Torneo_Misto (idTorneo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT fk_Torneo_Misto_has_Torneo_Torneo1
		FOREIGN KEY (idTorneo_allitaliana)
		REFERENCES db_progetto.Torneo (idTorneo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table db_progetto.Gara
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS db_progetto.Gara (
	idGara BIGINT NOT NULL AUTO_INCREMENT,
	idTorneo BIGINT NOT NULL,
	data DATE NULL,
	fase INT NULL COMMENT 'Se fa parte di un torneo ad eliminazione, indicherà di che fase fa parte la gara',
	PRIMARY KEY (idGara, idTorneo),
	INDEX fk_Gara_Torneo1_idx (idTorneo ASC),
	CONSTRAINT fk_Gara_Torneo1
		FOREIGN KEY (idTorneo)
		REFERENCES db_progetto.Torneo (idTorneo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table db_progetto.Iscritto_a
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS db_progetto.Iscritto_a (
	idGiocatore BIGINT NOT NULL,
	idTorneo BIGINT NOT NULL,
	approvata TINYINT(1) NOT NULL DEFAULT 0,
	sconto_rincaro TINYINT NOT NULL DEFAULT 0 COMMENT 'Se positivo è la percentuale di rincaro, se negativo la percentuale di sconto',
	PRIMARY KEY (idGiocatore, idTorneo),
	INDEX fk_Giocatore_has_Torneo_Torneo1_idx (idTorneo ASC),
	INDEX fk_Giocatore_has_Torneo_Giocatore1_idx (idGiocatore ASC),
	CONSTRAINT fk_Giocatore_has_Torneo_Giocatore1
		FOREIGN KEY (idGiocatore)
		REFERENCES db_progetto.Giocatore (idUtente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT fk_Giocatore_has_Torneo_Torneo1
		FOREIGN KEY (idTorneo)
		REFERENCES db_progetto.Torneo (idTorneo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table db_progetto.Ha_vinto
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS db_progetto.Ha_vinto (
	idGiocatore BIGINT NOT NULL,
	idTorneo BIGINT NOT NULL,
	PRIMARY KEY (idGiocatore, idTorneo),
	INDEX fk_Giocatore_has_Torneo_Torneo2_idx (idTorneo ASC),
	INDEX fk_Giocatore_has_Torneo_Giocatore2_idx (idGiocatore ASC),
	CONSTRAINT fk_Giocatore_has_Torneo_Giocatore2
		FOREIGN KEY (idGiocatore)
		REFERENCES db_progetto.Giocatore (idUtente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT fk_Giocatore_has_Torneo_Torneo2
		FOREIGN KEY (idTorneo)
		REFERENCES db_progetto.Torneo (idTorneo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table db_progetto.Partecipa_a
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS db_progetto.Partecipa_a (
	idGiocatore BIGINT NOT NULL,
	idGara BIGINT NOT NULL,
	idTorneo BIGINT NOT NULL,
	punteggio INT NULL,
	risultato INT NULL,
	PRIMARY KEY (idGiocatore, idGara, idTorneo),
	INDEX fk_Giocatore_has_Gara_Gara1_idx (idGara ASC, idTorneo ASC),
	INDEX fk_Giocatore_has_Gara_Giocatore1_idx (idGiocatore ASC),
	CONSTRAINT fk_Giocatore_has_Gara_Giocatore1
		FOREIGN KEY (idGiocatore)
		REFERENCES db_progetto.Giocatore (idUtente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT fk_Giocatore_has_Gara_Gara1
		FOREIGN KEY (idGara , idTorneo)
		REFERENCES db_progetto.Gara (idGara , idTorneo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table db_progetto.Commento
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS db_progetto.Commento (
	idCommento BIGINT NOT NULL AUTO_INCREMENT,
	idGiocatore BIGINT NOT NULL,
	idGara BIGINT NOT NULL,
	idTorneo BIGINT NOT NULL,
	testo TINYTEXT NOT NULL,
	data TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	idCommento_parent BIGINT NULL,
	PRIMARY KEY (idCommento),
	INDEX fk_Giocatore_has_Gara_Gara2_idx (idGara ASC, idTorneo ASC),
	INDEX fk_Giocatore_has_Gara_Giocatore2_idx (idGiocatore ASC),
	INDEX fk_Commento_Commento1_idx (idCommento_parent ASC),
	UNIQUE INDEX idCommento_UNIQUE (idCommento ASC),
	CONSTRAINT fk_Giocatore_has_Gara_Giocatore2
		FOREIGN KEY (idGiocatore)
		REFERENCES db_progetto.Giocatore (idUtente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT fk_Giocatore_has_Gara_Gara2
		FOREIGN KEY (idGara , idTorneo)
		REFERENCES db_progetto.Gara (idGara , idTorneo)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
	CONSTRAINT fk_Commento_Commento1
		FOREIGN KEY (idCommento_parent)
		REFERENCES db_progetto.Commento (idCommento)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table db_progetto.Utente_bannato
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS db_progetto.Utente_bannato (
	idUtente BIGINT NOT NULL,
	INDEX fk_Utente_bannato_Utente1_idx (idUtente ASC),
	PRIMARY KEY (idUtente),
	CONSTRAINT fk_Utente_bannato_Utente1
		FOREIGN KEY (idUtente)
		REFERENCES db_progetto.Utente (idUtente)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)
ENGINE = InnoDB;
