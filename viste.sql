CREATE OR REPLACE VIEW Classifica
	AS 
		SELECT P.idTorneo, P.idGiocatore, IFNULL(SUM(P.punteggio), 0) AS punti
		FROM Partecipa_a AS P JOIN Torneo AS T
			ON P.idTorneo = T.idTorneo
		GROUP BY P.idTorneo, P.idGiocatore;
