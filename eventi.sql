DELIMITER //
CREATE EVENT fine_giornata
	ON SCHEDULE EVERY 1 DAY
	STARTS CURRENT_DATE()
	COMMENT 'Controlla giornalmente quali tornei sono generabili e li genera'
	DO
		BEGIN 
			CALL genera_tornei_chiusi();
			CALL controlla_gare_tornei(); 
		END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE genera_tornei_chiusi()
BEGIN
	DECLARE idTor SERIAL;
	DECLARE done BOOLEAN;

	DECLARE tornei_curs CURSOR FOR
		SELECT T.idTorneo
		FROM Torneo AS T
		WHERE T.struttura IN (1, 2, 3) AND 
			(T.scadenza_iscr IS NULL OR T.scadenza_iscr < now()) AND
			T.idTorneo NOT IN (SELECT idTorneo FROM Gara) AND
			(	T.idTorneo NOT IN (SELECT idTorneo FROM Torneo_Misto) OR
				T.idTorneo NOT IN (SELECT idTorneo_Misto FROM Possiede_girone))
			AND
			T.idTorneo NOT IN (SELECT idTorneo FROM Ha_vinto);
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	CALL log('Eseguito evento fine giornata');

	OPEN tornei_curs;

	gen_tornei: LOOP
		FETCH tornei_curs INTO idTor;

		IF done = TRUE THEN
			LEAVE gen_tornei;
		END IF;

		CALL genera_torneo(idTor, @ret);
	END LOOP gen_tornei;               

	CLOSE tornei_curs;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE controlla_gare_tornei()
BEGIN
	DECLARE idTor SERIAL;
	DECLARE done BOOLEAN;

	DECLARE tornei_curs CURSOR FOR
		SELECT DISTINCT idTorneo
		FROM Partecipa_a
		WHERE idTorneo NOT IN (
			SELECT idTorneo
			FROM Partecipa_a
			WHERE risultato IS NULL OR 
				punteggio IS NULL
		) AND idTorneo NOT IN (
			SELECT idTorneo FROM Ha_vinto
		);
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN tornei_curs;

	gen_gare: LOOP
		FETCH tornei_curs INTO idTor;

		IF done = TRUE THEN
			LEAVE gen_gare;
		END IF;

		CALL gare_terminate(idTor);
	END LOOP gen_gare;

	CLOSE tornei_curs;

END //
DELIMITER ;
