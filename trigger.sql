DELIMITER //
CREATE TRIGGER controlla_vincoli_gara_u 
BEFORE UPDATE ON Partecipa_a
FOR EACH ROW
BEGIN
	DECLARE min INTEGER;
	DECLARE max INTEGER;
	DECLARE num_gioc INTEGER;
	DECLARE fase_gara INTEGER;
	DECLARE fase_max INTEGER;

	IF (SELECT DISTINCT idGiocatore
			FROM Iscritto_a
			WHERE idGiocatore = NEW.idGiocatore
				AND approvata = true) IS NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Gara non valida: il giocatore non e\' iscritto';
	END IF;

	SELECT COUNT(idGiocatore) INTO num_gioc
	FROM Partecipa_a AS P
	WHERE P.idTorneo = NEW.idTorneo AND P.idGara = NEW.idGara
	GROUP BY P.idTorneo, P.idGara;

	SELECT min_partecipanti, max_partecipanti INTO min, max
	FROM Torneo AS T
	WHERE T.idTorneo = NEW.idTorneo;

	IF num_gioc < min OR num_gioc > max THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Gara non valida: numero partecipanti invalido';
	END IF;

	SET num_gioc = 0;

	SELECT COUNT(P.idGiocatore) INTO num_gioc
	FROM Partecipa_a AS P
	WHERE P.idTorneo = idTorneo AND P.idGara = idGara
		AND P.idGiocatore NOT IN (
			SELECT idGiocatore
			FROM Iscritto_a AS I
			WHERE I.idTorneo = idTorneo
				AND approvata = true
		)
	GROUP BY P.idTorneo, P.idGara;

	IF num_gioc > 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Gara non valida: sono presenti giocatori non iscritti';
	END IF;


	IF NEW.punteggio <> OLD.punteggio OR NEW.risultato <> OLD.risultato THEN
		IF EXISTS(SELECT idTorneo FROM Ha_vinto WHERE idTorneo=OLD.idTorneo) THEN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Aggiornamento gara non consentito';
		END IF;

		SELECT MAX(fase) INTO fase_max
		FROM Gara
		WHERE idTorneo=OLD.idTorneo;

		SELECT fase INTO fase_gara
		FROM Gara
		WHERE idGara=OLD.idGara AND idTorneo=OLD.idTorneo;

		IF fase_gara IS NOT NULL AND fase_max > fase_gara THEN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Aggiornamento gara non consentito';
		END IF;
	END IF;
END //
DELIMITER ;


DELIMITER //
CREATE TRIGGER controlla_vincoli_gara
BEFORE INSERT ON Partecipa_a
FOR EACH ROW
BEGIN
	DECLARE min INTEGER;
	DECLARE max INTEGER;
	DECLARE num_gioc INTEGER;
	DECLARE num_gare INTEGER;
	DECLARE num_gare_limite INTEGER;

	IF (SELECT DISTINCT idGiocatore
			FROM Iscritto_a
			WHERE idGiocatore = NEW.idGiocatore
				AND approvata = true) IS NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Gara non valida: il giocatore non e\' iscritto';
	END IF;

	SET num_gioc = 0;

	SELECT COUNT(P.idGiocatore) INTO num_gioc
	FROM Partecipa_a AS P
	WHERE P.idTorneo = idTorneo AND P.idGara = idGara
		AND P.idGiocatore NOT IN (
			SELECT idGiocatore
			FROM Iscritto_a AS I
			WHERE I.idTorneo = idTorneo
				AND approvata = true
		)
	GROUP BY P.idTorneo, P.idGara;

	IF num_gioc > 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Gara non valida: sono presenti giocatori non iscritti';
	END IF;
END //
DELIMITER ;


DELIMITER //
CREATE TRIGGER controlla_numero_gare
BEFORE INSERT ON Gara
FOR EACH ROW
BEGIN
	DECLARE num_gare INTEGER;
	DECLARE num_gare_limite INTEGER;

	SELECT T.num_gare INTO num_gare_limite
	FROM Torneo AS T
	WHERE T.idTorneo = NEW.idTorneo;

	SELECT COUNT(G.idGara) INTO num_gare
	FROM Gara AS G
	WHERE G.idTorneo = NEW.idTorneo
	GROUP BY G.idTorneo;


	IF num_gare = num_gare_limite THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Impossibile inserire gare, numero massimo di gare raggiunto';
	END IF;
END //
DELIMITER ;


DELIMITER //
CREATE TRIGGER chk_Partecipanti
BEFORE INSERT ON Torneo
FOR EACH ROW
BEGIN
	IF NEW.min_partecipanti < 0 OR 
	NEW.max_partecipanti < NEW.min_partecipanti THEN
		SIGNAL SQLSTATE '23000'
			SET MESSAGE_TEXT = 'Violazione vincolo su numero partecipanti.';
	END IF;
END //
DELIMITER ;


DELIMITER //
CREATE TRIGGER chk_Partecipanti_u
BEFORE UPDATE ON Torneo
FOR EACH ROW
BEGIN
	IF NEW.min_partecipanti < 0 OR 
	NEW.max_partecipanti < NEW.min_partecipanti THEN
		SIGNAL SQLSTATE '23000'
			SET MESSAGE_TEXT = 'Violazione vincolo su numero partecipanti.';
	END IF;
END //
DELIMITER ;


DELIMITER //
CREATE TRIGGER chk_Commento_parent
BEFORE INSERT ON Commento
FOR EACH ROW
BEGIN
	DECLARE idTorneo SERIAL;
	DECLARE idGara SERIAL;

	IF NEW.idCommento_parent IS NOT NULL THEN
		SELECT C.idTorneo, C.idGara Into idTorneo, idGara
		FROM Commento AS C
		WHERE C.idCommento = NEW.idCommento_parent;

		IF idTorneo IS NULL OR idGara IS NULL THEN
			SIGNAL SQLSTATE '23000'
				SET MESSAGE_TEXT = 'Commento parent inesistente';
		END IF;

		IF idTorneo != NEW.idTorneo OR idGara != NEW.idGara THEN
			SIGNAL SQLSTATE '23000'
				SET MESSAGE_TEXT = 'Commento parent invalido';
		END IF;
	END IF;
END //
DELIMITER ;


DELIMITER //
CREATE TRIGGER chk_Commento_parent_u
BEFORE UPDATE ON Commento
FOR EACH ROW
BEGIN
	DECLARE idTorneo SERIAL;
	DECLARE idGara SERIAL;

	IF NEW.idCommento_parent IS NOT NULL THEN
		SELECT C.idTorneo, C.idGara Into idTorneo, idGara
		FROM Commento AS C
		WHERE C.idCommento = NEW.idCommento_parent;

		IF idTorneo IS NULL OR idGara IS NULL THEN
			SIGNAL SQLSTATE '23000'
				SET MESSAGE_TEXT = 'Commento parent inesistente';
		END IF;

		IF idTorneo != NEW.idTorneo OR idGara != NEW.idGara THEN
			SIGNAL SQLSTATE '23000'
				SET MESSAGE_TEXT = 'Commento parent invalido';
		END IF;
	END IF;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER controlla_vincoli_iscrizione
BEFORE DELETE ON Iscritto_a
FOR EACH ROW
BEGIN
	IF EXISTS (SELECT idGiocatore FROM Partecipa_a	WHERE idGiocatore = OLD.idGiocatore AND 
						idTorneo = OLD.idTorneo) THEN
		SIGNAL SQLSTATE '23000'
			SET MESSAGE_TEXT = 'Impossibile rimuovere giocatore';
	END IF;
END //
DELIMITER ;
