DELIMITER //
CREATE PROCEDURE crea_torneo(user varchar(20), 
				nome varchar(45), scadenza_iscr date, quota_iscr decimal(5,2),
				num_gare INTEGER /* NULL se calcolabile */, struttura INTEGER,
				min_partecipanti INTEGER, max_partecipanti INTEGER, premio varchar(45),
				riedizione_di SERIAL /* NULL se non lo è */,
				num_gironi INTEGER /* ignorato se non misto */)
BEGIN
	DECLARE idOrg SERIAL;
	DECLARE idTorneo SERIAL;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION ROLLBACK;
	DECLARE EXIT HANDLER FOR SQLWARNING ROLLBACK;

	SELECT Utente.idUtente INTO idOrg
	FROM Utente NATURAL JOIN Giocatore AS G
	WHERE Utente.user = user AND G.organizzatore = TRUE;

	IF struttura <> 4 THEN
		SET num_gare = NULL;
	END IF;

	START TRANSACTION;

	IF idOrg IS NOT NULL THEN
		INSERT INTO Torneo(nome, scadenza_iscr, quota_iscr, num_gare, 
				struttura, min_partecipanti,
				max_partecipanti, idTorneo_precedente, 
				idOrganizzatore, premio)
			VALUES(nome, scadenza_iscr, quota_iscr, num_gare, 
				struttura, min_partecipanti,
				max_partecipanti, riedizione_di, idOrg, premio);

		IF struttura = 3/* MISTO */ THEN
			SET idTorneo = LAST_INSERT_ID();
			INSERT INTO Torneo_Misto(idTorneo, num_gironi) VALUES(idTorneo, num_gironi);
		END IF;
	END IF;

	COMMIT;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE crea_gironi(idTorneo SERIAL, num_gironi INTEGER)
BEGIN
	DECLARE min INTEGER;
	DECLARE max INTEGER;
	DECLARE idOrg SERIAL;

	DECLARE idGirone SERIAL;
	DECLARE counter INTEGER DEFAULT 1;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION ROLLBACK;
	DECLARE EXIT HANDLER FOR SQLWARNING ROLLBACK;

	SELECT T.min_partecipanti, T.max_partecipanti, T.idOrganizzatore INTO min, max, idOrg
	FROM Torneo AS T
	WHERE T.idTorneo = idTorneo;

	START TRANSACTION;

	WHILE counter <= num_gironi DO
		INSERT INTO Torneo(struttura, min_partecipanti, max_partecipanti, idOrganizzatore)
			VALUES(1, min, max, idOrg);
		SET idGirone = LAST_INSERT_ID();

		INSERT INTO Possiede_girone VALUES(idTorneo, idGirone, counter);
		SET counter = counter + 1;
	END WHILE;

	COMMIT;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE controlla_gironi_misto(idTorn SERIAL)
BEGIN
	DECLARE num_gironi INTEGER;
	DECLARE gironi_conclusi INTEGER;

	DECLARE min INTEGER;
	DECLARE max INTEGER;
	DECLARE idOrg SERIAL;
	DECLARE idTorneoElim SERIAL;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION ROLLBACK;
	DECLARE EXIT HANDLER FOR SQLWARNING ROLLBACK;

	SELECT COUNT(P.idTorneo_allitaliana) INTO num_gironi
	FROM Possiede_girone AS P
	WHERE P.idTorneo_Misto = idTorn;

	SELECT COUNT(DISTINCT H.idTorneo) INTO gironi_conclusi
	FROM Ha_vinto AS H
	WHERE H.idTorneo IN (SELECT P.idTorneo_allitaliana
				FROM Possiede_girone AS P
				WHERE P.idTorneo_Misto = idTorn);

	START TRANSACTION;
	IF num_gironi = gironi_conclusi THEN

		SELECT T.min_partecipanti, T.max_partecipanti, T.idOrganizzatore INTO min, max, idOrg
		FROM Torneo AS T
		WHERE T.idTorneo = idTorn;


		INSERT INTO Torneo(struttura, min_partecipanti, max_partecipanti, idOrganizzatore, quota_iscr)
			VALUES(2, min, max, idOrg, 0);
		SET idTorneoElim = LAST_INSERT_ID();

		UPDATE Torneo_Misto SET idTorneo_Eliminazione=idTorneoElim
		WHERE Torneo_Misto.idTorneo = idTorn;

		INSERT INTO Iscritto_a(idGiocatore, idTorneo, approvata)
			SELECT H.idGiocatore, idTorneoElim, TRUE
			FROM Ha_vinto AS H
			WHERE H.idTorneo IN (SELECT P.idTorneo_allitaliana
						FROM Possiede_girone AS P
						WHERE P.idTorneo_Misto = idTorn
			);
	END IF;
	COMMIT;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE genera_torneo(idTorneo SERIAL,
			OUT ret BOOLEAN)
BEGIN
	DECLARE struttura INT;
	DECLARE num_part INT;
	DECLARE minimo INT;

	SELECT COUNT(*) INTO num_part
	FROM Iscritto_a AS I
	WHERE
		I.idTorneo = idTorneo AND
		I.approvata = TRUE;

	SELECT T.struttura, T.min_partecipanti INTO struttura, minimo
	FROM Torneo AS T
	WHERE T.idTorneo = idTorneo;

	IF minimo > num_part THEN
		SET @ret = FALSE;
	ELSEIF struttura = 1 THEN /* All'italiana */
		CALL genera_torneo_italiana(idTorneo, num_part, @ret);
	ELSEIF struttura = 2 THEN /* Eliminazione */
		CALL genera_torneo_eliminazione(idTorneo, num_part, @ret);
	ELSEIF struttura = 3 THEN /* Misto */
		CALL genera_torneo_misto(idTorneo, num_part, @ret);
	ELSE
		SET @ret = FALSE;
	END IF;

	SET ret = @ret;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE genera_torneo_italiana(idTorneo SERIAL, num_part INT,
			OUT ret BOOLEAN)
BEGIN  
	DECLARE min INT;
	DECLARE max INT;
	DECLARE limite INT;

	DECLARE done BOOLEAN;
	DECLARE idGioc1 SERIAL;
	DECLARE idGioc2 SERIAL;
	DECLARE idGara SERIAL;
	DECLARE data DATE DEFAULT NOW();

	DECLARE gioc_curs CURSOR FOR
		SELECT G.idGiocatore
		FROM Iscritto_a AS G
		WHERE G.idTorneo = idTorneo;

	DECLARE coppie_curs CURSOR FOR
		SELECT A.idGiocatore, B.idGiocatore
		FROM Iscritto_a A JOIN Iscritto_a B
			ON  A.idGiocatore < B.idGiocatore
		WHERE A.idTorneo = idTorneo AND B.idTorneo = idTorneo
		ORDER BY RAND();
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION ROLLBACK;
	DECLARE EXIT HANDLER FOR SQLWARNING ROLLBACK;

	SELECT min_partecipanti, max_partecipanti INTO min, max
	FROM Torneo AS T
	WHERE T.idTorneo = idTorneo;

	START TRANSACTION;
	IF max > 2 THEN
		UPDATE Torneo AS T SET T.num_gare = num_part
			WHERE T.idTorneo = idTorneo;
		SET limite = max - 1;

		OPEN gioc_curs;

		crea_gareN: LOOP
			FETCH gioc_curs INTO idGioc1;

			IF done = TRUE THEN
				LEAVE crea_gareN;
			END IF;

			SET data = ADDDATE(data, INTERVAL 1 DAY);
			INSERT INTO Gara(idTorneo, data)
				VALUES (idTorneo, data);
			SET idGara = LAST_INSERT_ID();

			INSERT INTO Partecipa_a(idGiocatore, idGara, idTorneo)
				VALUES(idGioc1, idGara, idTorneo);

			/* Scegli max-1 giocatori casuali distinti diversi da idGioc1 */
			INSERT INTO Partecipa_a(idGiocatore, idGara, idTorneo)
				SELECT I.idGiocatore, idGara, idTorneo
				FROM Iscritto_a AS I
				WHERE I.idTorneo = idTorneo AND I.idGiocatore <> idGioc1
				ORDER BY RAND()
				LIMIT limite;
		END LOOP crea_gareN;

		CLOSE gioc_curs;
		SET ret = TRUE;
	ELSE
		UPDATE Torneo AS T SET T.num_gare = (num_part*(num_part - 1)) / 2
		WHERE T.idTorneo = idTorneo;
		OPEN coppie_curs;

		crea_gare: LOOP
			FETCH coppie_curs INTO idGioc1, idGioc2;

			IF done = TRUE THEN
				LEAVE crea_gare;
			END IF;

			SET data = ADDDATE(data, INTERVAL 1 DAY);
			INSERT INTO Gara(idTorneo, data)
				VALUES (idTorneo, data);
			SET idGara = LAST_INSERT_ID();

			INSERT INTO Partecipa_a(idGiocatore, idGara, idTorneo)
				VALUES(idGioc1, idGara, idTorneo);
			INSERT INTO Partecipa_a(idGiocatore, idGara, idTorneo)
				VALUES(idGioc2, idGara, idTorneo);

		END LOOP crea_gare;

		CLOSE coppie_curs;
		SET ret = TRUE;
	END IF;
	IF ret = FALSE THEN
		ROLLBACK;
	ELSE
		COMMIT;
	END IF;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE genera_torneo_eliminazione(idTorneo SERIAL, num_part INT,
			OUT ret BOOLEAN)
BEGIN
	DECLARE min INT;
	DECLARE max INT;

	SELECT min_partecipanti, max_partecipanti INTO min, max
	FROM Torneo AS T
	WHERE T.idTorneo = idTorneo;

	CALL genera_fase(idTorneo, 0, num_part, min, max, @ret);
	SET ret = @ret;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE genera_fase(idTorneo SERIAL, fase INT, num_part INT, min INT, max INT,
			OUT ret BOOLEAN)
BEGIN
	DECLARE idGara SERIAL;
	DECLARE data DATE DEFAULT NOW();

	DECLARE numGare INTEGER;
	DECLARE counter INTEGER DEFAULT 0;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION ROLLBACK;
	DECLARE EXIT HANDLER FOR SQLWARNING ROLLBACK;

	START TRANSACTION;

	SET ret = TRUE;

	IF fase > 0 THEN
		/* numero di giocatori che passano alla fase succesiva */
		SELECT COUNT(*) INTO num_part
		FROM Partecipa_a AS P JOIN Gara AS G
			ON P.idGara = G.idGara AND P.idTorneo = G.idTorneo
		WHERE G.idTorneo = idTorneo AND
			G.fase = fase AND
			P.punteggio >= ALL (
				SELECT P2.punteggio
				FROM Partecipa_a AS P2 
				WHERE P2.idGara = G.idGara AND P2.idTorneo = G.idTorneo
		);
	END IF;

	SET numGare = num_part DIV max;
	IF num_part % max <> 0 THEN
		IF num_part % max >= min THEN
			SET numGare = numGare + 1;
		ELSE
			/* non si può fare il torneo, quindi chi rimane a vinto! */
			SET ret = FALSE;
		END IF;
	END IF;

	WHILE counter < numGare AND ret = TRUE DO
		/* crea gara */
		SET data = ADDDATE(data, INTERVAL 1 DAY);
		INSERT INTO Gara(idTorneo, data, fase)
			VALUES (idTorneo, data, fase + 1);
		SET idGara = LAST_INSERT_ID();

		CALL elim_inserisci_giocatori(idTorneo, idGara, fase, counter*max, max);

		SET counter = counter + 1;
	END WHILE;
	IF ret = FALSE THEN
		ROLLBACK;
	ELSE
		COMMIT;
	END IF;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE elim_inserisci_giocatori(idTorneo SERIAL, idGaraSucc SERIAL, fase_attuale INTEGER, 
			offset INTEGER, num_part INTEGER)
BEGIN
	IF fase_attuale = 0 THEN
		INSERT INTO Partecipa_a(idGiocatore, idGara, idTorneo)
			SELECT idGiocatore, idGaraSucc, idTorneo
			FROM Iscritto_a AS I
			WHERE I.idTorneo = idTorneo AND approvata = TRUE
			LIMIT offset, num_part;
	ELSE
		INSERT INTO Partecipa_a(idGiocatore, idGara, idTorneo)
			SELECT idGiocatore, idGaraSucc, idTorneo
			FROM Partecipa_a AS P JOIN Gara AS G
				ON P.idGara = G.idGara AND P.idTorneo = G.idTorneo
			WHERE G.idTorneo = idTorneo AND
				G.fase = fase_attuale AND
				P.punteggio >= ALL (
					SELECT P2.punteggio
					FROM Partecipa_a AS P2 
					WHERE P2.idGara = G.idGara AND P2.idTorneo = G.idTorneo
			)
			LIMIT offset, num_part;
	END IF;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE genera_torneo_misto(idTorneo SERIAL, num_part INT,
		OUT ret BOOLEAN)
BEGIN
	DECLARE num_gironi INTEGER;
	DECLARE part_per_girone INTEGER;
	DECLARE resto INTEGER;

	DECLARE counter INTEGER;
	DECLARE offset INTEGER;
	DECLARE idGirone SERIAL;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION ROLLBACK;
	DECLARE EXIT HANDLER FOR SQLWARNING ROLLBACK;

	SELECT T.num_gironi INTO num_gironi
	FROM Torneo_Misto AS T
	WHERE T.idTorneo = idTorneo;

	START TRANSACTION;
	CALL crea_gironi(idTorneo, num_gironi);

	SET part_per_girone = num_part DIV num_gironi;
	SET resto = num_part % num_gironi;

	SET offset = 0;
	SET counter = 1;
	SET @ret = TRUE;
	WHILE counter <= num_gironi AND @ret = TRUE DO
		SELECT idTorneo_allitaliana INTO idGirone
		FROM Possiede_girone
		WHERE idTorneo_Misto = idTorneo AND Girone = counter;

		IF counter = num_gironi THEN
			/* metto tutti i giocatori restanti nell'ultimo girone */
			SET part_per_girone = part_per_girone + resto;
		END IF;

		INSERT INTO Iscritto_a(idGiocatore, idTorneo, approvata)
			SELECT idGiocatore, idGirone, TRUE
			FROM Iscritto_a AS I
			WHERE I.idTorneo = idTorneo AND approvata = TRUE
			LIMIT offset, part_per_girone;

		CALL genera_torneo_italiana(idGirone, part_per_girone, @ret);

		SET offset = offset + part_per_girone;
		SET counter = counter + 1;
	END WHILE;

	SET ret = @ret;
	IF ret = FALSE THEN
		ROLLBACK;
	ELSE
		COMMIT;
	END IF;
END //
DELIMITER ;



DELIMITER //
CREATE PROCEDURE genera_fase_successiva(idTorneo SERIAL, OUT ret BOOLEAN)
BEGIN
	DECLARE min INTEGER;
	DECLARE max INTEGER;
	DECLARE fase INTEGER;

	SELECT min_partecipanti, max_partecipanti INTO min, max
	FROM Torneo AS T
	WHERE T.idTorneo = idTorneo;

	SELECT MAX(G.fase) INTO fase
	FROM Gara AS G
	WHERE G.idTorneo = idTorneo;

	CALL genera_fase(idTorneo, fase, 0/*non importante*/, min, max, @ret);
	SET ret = @ret;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE dichiara_vincitori(idTorneo SERIAL)
BEGIN
	/* I vincitori sono tutti coloro che possiedono il punteggio maggiore */
	INSERT INTO Ha_vinto(idGiocatore, idTorneo)
		SELECT C.idGiocatore, idTorneo
		FROM Classifica AS C
		WHERE C.idTorneo = idTorneo AND 
			C.punti >= ALL (
				SELECT MAX(C2.punti)
				FROM Classifica AS C2
				WHERE C2.idTorneo = idTorneo
		);
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE gare_terminate(idTorneo SERIAL)
BEGIN
	DECLARE idMisto SERIAL;
	DECLARE struttura INTEGER;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION ROLLBACK;
	DECLARE EXIT HANDLER FOR SQLWARNING ROLLBACK;

	SELECT T.struttura INTO struttura
	FROM Torneo AS T
	WHERE T.idTorneo = idTorneo;

	START TRANSACTION;
	IF struttura = 1 THEN
		CALL dichiara_vincitori(idTorneo);

		SELECT idTorneo_Misto INTO idMisto
		FROM Possiede_girone
		WHERE idTorneo_allitaliana = idTorneo;

		IF idMisto IS NOT NULL THEN
			CALL controlla_gironi_misto(idMisto);
		END IF;
	ELSEIF struttura = 2 THEN
		CALL genera_fase_successiva(idTorneo, @ret);
		IF @ret = FALSE THEN
			CALL dichiara_vincitori(idTorneo);

			SELECT T.idTorneo INTO idMisto
			FROM Torneo_Misto AS T
			WHERE T.idTorneo_Eliminazione = idTorneo;

			IF idMisto IS NOT NULL THEN
				INSERT INTO Ha_vinto(idGiocatore, idTorneo)
					SELECT H.idGiocatore, idMisto
					FROM Ha_vinto AS H
					WHERE H.idTorneo = idTorneo;
			END IF;
		END IF;
	ELSEIF struttura = 4 THEN
		CALL dichiara_vincitori(idTorneo);
	END IF;

	COMMIT;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE check_gara(idTorneo SERIAL, idGara SERIAL)
BEGIN
	DECLARE min INTEGER;
	DECLARE max INTEGER;
	DECLARE num_gioc INTEGER;

	SELECT T.min_partecipanti, T.max_partecipanti INTO min, max
	FROM Torneo AS T
	WHERE T.idTorneo = idTorneo;

	IF min IS NULL OR max IS NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Gara non valida: numero partecipanti non definito';
	END IF;

	SELECT COUNT(P.idGiocatore) INTO num_gioc
	FROM Partecipa_a AS P
	WHERE P.idTorneo = idTorneo AND P.idGara = idGara
	GROUP BY P.idTorneo, P.idGara;

	IF num_gioc IS NULL OR num_gioc < min OR num_gioc > max THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Gara non valida: numero partecipanti invalido';
	END IF;

	SET num_gioc = 0;

	SELECT COUNT(P.idGiocatore) INTO num_gioc
	FROM Partecipa_a AS P
	WHERE P.idTorneo = idTorneo AND P.idGara = idGara
		AND P.idGiocatore NOT IN (
			SELECT idGiocatore
			FROM Iscritto_a AS I
			WHERE I.idTorneo = idTorneo
				AND approvata = true
		)
	GROUP BY P.idTorneo, P.idGara;

	IF num_gioc > 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Gara non valida: sono presenti giocatori non iscritti';
	END IF;
END //
DELIMITER ;
