-- drop procedure Utenti 
DROP PROCEDURE IF EXISTS aggiungi_amministratore;
DROP PROCEDURE IF EXISTS rimuovi_amministratore;
DROP PROCEDURE IF EXISTS registra_utente;
DROP PROCEDURE IF EXISTS rendi_giocatore;
DROP PROCEDURE IF EXISTS rendi_organizzatore;
DROP PROCEDURE IF EXISTS rimuovi_organizzatore;
DROP PROCEDURE IF EXISTS ban_utente;
DROP PROCEDURE IF EXISTS revoca_ban_utente;
DROP FUNCTION IF EXISTS esiste_utente;
DROP PROCEDURE IF EXISTS iscrivi_a;
DROP PROCEDURE IF EXISTS approva_iscrizione;
DROP FUNCTION IF EXISTS tipo_utente;
DROP FUNCTION IF EXISTS posizione_nel_torneo;
DROP PROCEDURE IF EXISTS aggiungi_commento;

-- drop procedure Tornei 
DROP PROCEDURE IF EXISTS crea_torneo;
DROP PROCEDURE IF EXISTS crea_gironi;
DROP PROCEDURE IF EXISTS controlla_gironi_misto;
DROP PROCEDURE IF EXISTS genera_torneo;
DROP PROCEDURE IF EXISTS genera_torneo_italiana;
DROP PROCEDURE IF EXISTS genera_torneo_eliminazione;
DROP PROCEDURE IF EXISTS genera_fase;
DROP PROCEDURE IF EXISTS elim_inserisci_giocatori;
DROP PROCEDURE IF EXISTS genera_torneo_misto;
DROP PROCEDURE IF EXISTS genera_fase_successiva;
DROP PROCEDURE IF EXISTS dichiara_vincitori;
DROP PROCEDURE IF EXISTS gare_terminate;
DROP PROCEDURE IF EXISTS check_gara;

-- drop trigger
DROP TRIGGER IF EXISTS controlla_vincoli_gara;
DROP TRIGGER IF EXISTS controlla_vincoli_gara_u;
DROP TRIGGER IF EXISTS controlla_numero_gare;
DROP TRIGGER IF EXISTS chk_Partecipanti;
DROP TRIGGER IF EXISTS chk_Partecipanti_u;
DROP TRIGGER IF EXISTS chk_Commento_parent;
DROP TRIGGER IF EXISTS chk_Commento_parent_u;
DROP TRIGGER IF EXISTS controlla_vincoli_iscrizione;

-- drop view
DROP VIEW IF EXISTS Classifica;

-- drop event
DROP EVENT IF EXISTS fine_giornata;

DROP PROCEDURE IF EXISTS genera_tornei_chiusi;
DROP PROCEDURE IF EXISTS controlla_gare_tornei;

-- drop table

