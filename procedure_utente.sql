DELIMITER //
CREATE PROCEDURE aggiungi_amministratore(user varchar(20))
BEGIN
	DECLARE id SERIAL;

	SELECT idUtente INTO id
	FROM Utente
	WHERE Utente.user = user;

	IF id IS NOT NULL THEN
		INSERT INTO Amministratore(idUtente, data_creazione) VALUES (id, NOW());
	END IF;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE rimuovi_amministratore(user varchar(20))
BEGIN
	DECLARE id SERIAL;

	SELECT idUtente INTO id
	FROM Utente
	WHERE Utente.user = user;

	IF id IS NOT NULL THEN
		DELETE FROM Amministratore WHERE idUtente = id;
	END IF;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE registra_utente(IN user varchar(20), IN pwd varchar(256),
		OUT ret BOOLEAN)
BEGIN
	DECLARE idUtente SERIAL;

	SELECT Utente.idUtente INTO idUtente
	FROM Utente
	WHERE Utente.user = user;

	IF idUtente IS NULL THEN
		INSERT INTO Utente(user, password) VALUES(user, SHA2(pwd, 256));
		SET ret = TRUE;
	ELSE
		SET ret = FALSE;
	END IF;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE rendi_giocatore(user varchar(20), nome varchar(50), cognome varchar(50))
BEGIN
	DECLARE idUtente SERIAL;

	SELECT Utente.idUtente INTO idUtente
	FROM Utente
	WHERE Utente.user = user;

	IF idUtente IS NOT NULL THEN
		INSERT INTO Giocatore VALUES(idUtente, nome, cognome, FALSE);
	END IF;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE rendi_organizzatore(user varchar(20))
BEGIN
	DECLARE idUtente SERIAL;

	SELECT Utente.idUtente INTO idUtente
	FROM Utente
	WHERE Utente.user = user;

	IF idUtente IS NOT NULL THEN
		UPDATE Giocatore SET organizzatore = TRUE WHERE Giocatore.idUtente = idUtente;
	END IF;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE rimuovi_organizzatore(user varchar(20))
BEGIN
	DECLARE idUtente SERIAL;

	SELECT Utente.idUtente INTO idUtente
	FROM Utente
	WHERE Utente.user = user;

	IF idUtente IS NOT NULL THEN
		UPDATE Giocatore SET organizzatore = FALSE WHERE Giocatore.idUtente = idUtente;
	END IF;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE ban_utente(user varchar(20))
BEGIN
	DECLARE idUtente SERIAL;

	SELECT Utente.idUtente INTO idUtente
	FROM Utente
	WHERE Utente.user = user;

	IF idUtente IS NOT NULL THEN
		INSERT INTO Utente_bannato(idUtente)
			VALUES(idUtente);
	END IF;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE revoca_ban_utente(user varchar(20))
BEGIN
	DECLARE idUtente SERIAL;

	SELECT Utente.idUtente INTO idUtente
	FROM Utente
	WHERE Utente.user = user;

	IF idUtente IS NOT NULL THEN
		DELETE FROM Utente_bannato
		WHERE Utente_bannato.idUtente=idUtente;
	END IF;
END //
DELIMITER ;


DELIMITER //
CREATE FUNCTION esiste_utente(user varchar(20), pwd varchar(256))
	RETURNS BOOLEAN
READS SQL DATA
BEGIN
	IF EXISTS (SELECT idUtente FROM Utente AS U WHERE U.user = user AND U.password = SHA2(pwd, 256)) THEN
		return TRUE;
	ELSE
		return FALSE;
    END IF;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE iscrivi_a(user varchar(20), idTorneo SERIAL, sconto_rincaro TINYINT(4))
BEGIN
	DECLARE idGiocatore SERIAL;
	DECLARE idOrg SERIAL;

	SELECT Utente.idUtente INTO idGiocatore
	FROM Utente
	WHERE Utente.user = user;

	SELECT idOrganizzatore INTO idOrg
	FROM Torneo AS T
	WHERE T.idTorneo = idTorneo;

	IF idGiocatore IS NOT NULL AND idTorneo IS NOT NULL THEN
		IF sconto_rincaro IS NULL THEN
			INSERT INTO Iscritto_a(idGiocatore, idTorneo)
				VALUES(idGiocatore, idTorneo);
		ELSE
			INSERT INTO Iscritto_a(idGiocatore, idTorneo, sconto_rincaro)
				VALUES(idGiocatore, idTorneo, sconto_rincaro);
		END IF;

		IF idOrg = idGiocatore THEN
			UPDATE Iscritto_a AS I SET approvata = TRUE 
				WHERE I.idGiocatore = idOrg AND
					I.idTorneo = idTorneo;
		END IF;
	END IF;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE approva_iscrizione(user varchar(20), nome_torneo varchar(45))
BEGIN
	DECLARE idGiocatore SERIAL;
	DECLARE idTorneo SERIAL;

	SELECT Utente.idUtente INTO idGiocatore
	FROM Utente
	WHERE Utente.user = user;

	SELECT Torneo.idTorneo INTO idTorneo
	FROM Torneo
	WHERE Torneo.nome = nome_torneo;

	IF idGiocatore IS NOT NULL AND idTorneo IS NOT NULL THEN
		UPDATE Iscritto_a AS I SET approvata = TRUE 
			WHERE I.idGiocatore = idGiocatore AND
				I.idTorneo = idTorneo;
	END IF;
END //
DELIMITER ;

DELIMITER //
CREATE FUNCTION tipo_utente(user VARCHAR(20))
	RETURNS INTEGER /* bitmask AOGUB */
	READS SQL DATA
BEGIN
	DECLARE id SERIAL;
	DECLARE org BOOLEAN;
	DECLARE perm INTEGER;

	SET perm = 0;

	SELECT U.idUtente INTO id
	FROM Utente AS U
	WHERE U.user = user;

	IF id IS NULL THEN
		RETURN 0; /* NON ESISTE */
	ELSE
		SET perm = perm | (1 << 1); /* Utente */
	END IF;

	IF (SELECT idUtente FROM Utente_bannato WHERE idUtente = id) IS NOT NULL THEN
		SET perm = perm | (1 << 0);
	END IF;

	IF (SELECT idUtente FROM Amministratore WHERE idUtente = id) IS NOT NULL THEN
		SET perm = perm | (1 << 4); /* Amministratore */
	END IF;

	SELECT organizzatore INTO org
	FROM Giocatore
	WHERE idUtente = id;

	IF org IS NOT NULL THEN
		SET perm = perm | (1 << 2); /* Giocatore */

		IF org = TRUE THEN
			SET perm = perm | (1 << 3); /* Organizzatore */
		END IF;
	END IF;

	RETURN perm;
END //
DELIMITER ;


DELIMITER //
CREATE FUNCTION posizione_nel_torneo(idGiocatore SERIAL, idTorneo SERIAL)
	RETURNS INT
	READS SQL DATA
BEGIN
	DECLARE idGioc SERIAL;
	DECLARE Posizione INTEGER;

	SELECT I.idGiocatore INTO idGioc
	FROM Iscritto_a AS I
	WHERE I.idTorneo = idTorneo AND
		I.idGiocatore = idGiocatore AND
		I.approvata = TRUE;
	
	IF idGioc IS NULL THEN
		RETURN 0;
	END IF;

	SELECT COUNT(DISTINCT C.punti) + 1  into Posizione
	FROM Classifica AS C
	WHERE C.idTorneo = idTorneo AND 
		C.punti > (
			SELECT punti 
			FROM Classifica AS C1
			WHERE C1.idGiocatore = idGiocatore AND
				C1.idTorneo = idTorneo
	);

	RETURN Posizione;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE aggiungi_commento(user varchar(20), idTorneo SERIAL, idGara SERIAL,
					commento TINYTEXT, idCommento_parent SERIAL)
BEGIN
	INSERT INTO Commento(idGiocatore, idGara, idTorneo, testo, idCommento_parent)
		SELECT idGiocatore, idGara, idTorneo, commento, idCommento_parent
		FROM Utente AS U JOIN Iscritto_a AS I
			ON U.idUtente = I.idGiocatore
		WHERE U.user = user AND I.idTorneo = idTorneo;
END //
DELIMITER ;
