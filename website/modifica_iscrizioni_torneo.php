<?php
session_start();
require_once('lib/functions.php');
$conn = open_connection();
$logged = null;
$user = null;
if(isset($_SESSION['is_logged'])){
    $logged = $_SESSION['is_logged'];
    $user = $_SESSION['user'];
}

if($logged){
    $_SESSION['is_logged'] = $logged;
    $_SESSION['user'] = $user;
}

if(!canCreateTournaments($conn, $user)) {
	header('HTTP/1.0 403 Forbidden');
	header('Location: /redirect.php');
	die();
}

if(isset($_GET['operazione']) && isset($_GET['gioc']) && isset($_GET['torneo'])) {
	if($_GET['operazione'] == 'accept') {
		subscribe_tournament($conn, $_GET['gioc'], $_GET['torneo']);
	} else if($_GET['operazione'] == 'refuse') {
		unsubscribe_tournament($conn, $_GET['gioc'], $_GET['torneo']);
	}
}
?>
<!DOCTYPE html>
<html>

<head>
<?php include_once('lib/header.php');?>
</head>

<body>
<?php include('lib/menu.php')?>
<h4>Pagina di modifiche iscrizioni al torneo</h4>
<form method="GET" action="<?php echo($_SERVER['PHP_SELF']); ?>">
<div class="uk-form-row">
	<select name="operazione">
		<option value="accept">Accetta iscrizione</option>
		<option value="refuse">Rifiuta iscrizione</option>
	</select>
</div>
<div class="uk-form-row">
	<input type="hidden" name="torneo" value="<?php echo($_GET['torneo'])?>"/>
	<button class="uk-button">Esegui</button>
</div>
<div class="uk-form-row">
<table class="uk-table uk-table-striped uk-table-hover">
	<thead>
		<tr>
			<th>Username</th>
			<th>Status iscrizione</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<?php
	$user_id = getUserID($conn, $user);
	$stmt = $conn->prepare('SELECT idGiocatore,user,approvata FROM Iscritto_a JOIN Utente ON idGiocatore=idUtente WHERE idTorneo=?');
	$stmt->bindParam(1, $_GET['torneo'], PDO::PARAM_STR, 20);
	$stmt->execute();
	$users = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($users as $row) {
		print('<tr>' . PHP_EOL);
		print('<td>' . $row['user'] . '</td>' . PHP_EOL);
		if($row['approvata'] == 1)
			print('<td>Approvata</td>' . PHP_EOL);
		else if($row['approvata'] == 0)
			print('<td>Non approvata</td>' . PHP_EOL);
		else
			print('<td>Indefinito</td>' . PHP_EOL);
		print('<td><input type="radio" name="gioc" value="' . $row['idGiocatore'] . '"></td>' . PHP_EOL);
		print('</tr>' . PHP_EOL);
	}
	unset($conn);
?>
	</tbody>
</table>
</div>

</form>
</body>
</html>
