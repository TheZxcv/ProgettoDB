<?php
session_start();
require_once('lib/functions.php');
$conn = open_connection();
$logged = null;
$user = null;
if(isset($_SESSION['is_logged'])){
    $logged = $_SESSION['is_logged'];
    $user = $_SESSION['user'];
}

if($logged){
    $_SESSION['is_logged'] = $logged;
    $_SESSION['user'] = $user;
}

if(!canBrowseWebsite($conn, $user)) {
	header('HTTP/1.0 403 Forbidden');
	header('Location: /redirect.php');
	die();
}
?>
<!DOCTYPE html>
<html>

<head>
<?php include_once('lib/header.php');?>
</head>

<body>
<?php include('lib/menu.php');?>
<table class="uk-table uk-table-striped uk-table-hover">
	<thead>
		<tr>
			<th>Nome Torneo</th>
			<th>Struttura</th>
			<th>Posizione</th>
			<th>Costo</th>
			<th>Premio</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<?php
	$stmt = $conn->prepare('SELECT T.nome, T.struttura, T.idTorneo, '.
				'T.quota_iscr*(1 - I.sconto_rincaro/100) AS costo, '.
				'IF(H.idGiocatore IS NULL, \'nessuno\', T.premio) AS premio '.
				'FROM Torneo AS T '.
				'	JOIN Iscritto_a AS I '.
				'		ON T.idTorneo=I.idTorneo '.
				'	LEFT JOIN Ha_vinto AS H '.
				'		ON H.idGiocatore=I.idGiocatore AND '.
				'			H.idTorneo=T.idTorneo '.
				'WHERE T.nome IS NOT NULL AND '.
				'	I.approvata = TRUE AND            '.
				'	I.idGiocatore IN (SELECT idUtente '.
				'			FROM Utente '.
				'			WHERE user = ?)'
			);
	$stmt->bindParam(1, $user, PDO::PARAM_STR, 20);
	$stmt->execute();
	$tornei = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($tornei as $row) {
		if($row['struttura'] == 3)
			print('<tr onclick="document.location = \'lista_gironi.php?torneo=' . $row['idTorneo'] . '\';">' . PHP_EOL);
		else
			print('<tr onclick="document.location = \'lista_gare.php?torneo=' . $row['idTorneo'] . '\';">' . PHP_EOL);
		print('<td>' . $row['nome'] . '</td>' . PHP_EOL);
		print('<td>' . getNomeStruttura($row['struttura']) . '</td>' . PHP_EOL);
		print('<td>' . getPlayerRank($conn, $user, $row['idTorneo']) . '</td>' . PHP_EOL);
		print('<td>€' . number_format((float)$row['costo'], 2, '.', '') . '</td>' . PHP_EOL);
		print('<td>' . $row['premio'] . '</td>' . PHP_EOL);
		if($row['struttura'] == 3)
			print('<td>Vedere la classifica dei gironi</td>' . PHP_EOL);
		else
			print('<td><a href="classifica.php?torneo=' . $row['idTorneo'] . '">Classifica</a></td>' . PHP_EOL);
		print('</tr>' . PHP_EOL);
	}
	$stmt->closeCursor();
	unset($conn);
?>
</tbody>
</table>
</body>
</html>
