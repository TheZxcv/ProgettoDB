<?php
session_start();
require_once('lib/functions.php');
$conn = open_connection();
$logged = null;
$user = null;
if(isset($_SESSION['is_logged'])){
    $logged = $_SESSION['is_logged'];
    $user = $_SESSION['user'];
}

if($logged){
    $_SESSION['is_logged'] = $logged;
    $_SESSION['user'] = $user;
}

if(!canCreateTournaments($conn, $user)) {
	header('HTTP/1.0 403 Forbidden');
	header('Location: /redirect.php');
	die();
}
?>
<!DOCTYPE html>
<html>

<head>
<?php include_once('lib/header.php');?>
</head>

<body>
<?php include('lib/menu.php');?>
<table class="uk-table uk-table-striped uk-table-hover">
	<thead>
		<tr>
			<th>Nome Torneo</th>
			<th>Struttura</th>
			<th>Premio</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<?php
	$stmt = $conn->prepare('SELECT nome, struttura, premio, idTorneo FROM Torneo WHERE nome IS NOT NULL AND idOrganizzatore =?');
	$stmt->bindParam(1, getUserID($conn, $user), PDO::PARAM_STR, 20);
	$stmt->execute();
	$tornei = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($tornei as $row) {
		if($row['struttura'] == 4)
			print('<tr onclick="document.location = \'crea_gara.php?torneo=' . $row['idTorneo'] . '\';">' . PHP_EOL);
		else
			print('<tr>' . PHP_EOL);
		print('<td>' . $row['nome'] . '</td>' . PHP_EOL);
		print('<td>' . getNomeStruttura($row['struttura']) . '</td>' . PHP_EOL);
		print('<td>' . $row['premio'] . '</td>' . PHP_EOL);
		if($row['struttura'] == 4)
			print('<td>Premere per aggiungere gara</td>' . PHP_EOL);
		else
			print('<td></td>' . PHP_EOL);
		print('</tr>' . PHP_EOL);
	}
	$stmt->closeCursor();
	unset($conn);
?>
	</tbody>
</table>
</body>
</html>
