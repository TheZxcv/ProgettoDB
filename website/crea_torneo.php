<?php // Login chek
ini_set("error reporting", E_ALL);
session_start();
include_once('lib/functions.php');
$conn = open_connection();
$logged = null;
$user = null;
//Verifico l'esistenza del cookie e di $logged
if(isset($_POST['user'])){
    $myuser=trim($_POST['user']);
    $mypsw=trim($_POST['psw']);
    $logged=check_login($conn, $myuser, $mypsw);
    $user = $myuser;
}
if(isset($_SESSION['is_logged'])){
    $logged=$_SESSION['is_logged'];
    $user=$_SESSION['user'];
}
if($logged){
    $_SESSION['is_logged']=$logged;
    $_SESSION['user']=$user;
}

if(!canCreateTournaments($conn, $user)) {
	header('HTTP/1.0 403 Forbidden');
	header('Location: /redirect.php');
	die();
}

if(isset($_POST['mod']) && $_POST['mod'] == 'out'){
    unset($_SESSION['is_logged']);
    unset($_SESSION['user']);
    $logged=null;
    unset($_SESSION['mod']);
}

if(isset($_POST['torneo'])){
	if($_POST['riedizione']=='') $_POST['riedizione'] = NULL;
	create_torneo($conn, $user, $_POST['torneo'], $_POST['scadenza'], 
		$_POST['quota'], $_POST['tipologia'], $_POST['min_partecipanti'],
		$_POST['max_partecipanti'], $_POST['premio'], $_POST['riedizione'],
		$_POST['num_gare'], $_POST['num_gironi']);
}
unset($conn);
?>
<!DOCTYPE html>
<html>

<head>
<?php include_once('lib/header.php');?>
	<link href="uikit/css/components/datepicker.css" rel="stylesheet" type="text/css">
	<script src="uikit/js/components/datepicker.min.js"></script>
</head>

<body>
<?php
ini_set("error reporting", E_ALL);
include('lib/menu.php');
?>

<form class="uk-form uk-form-horizontal" method="POST" action="<?php echo($_SERVER['PHP_SELF']);?>">
    <fieldset data-uk-margin>
        <legend>Inserire i dati per creare il torneo</legend>
        <div class="uk-form-row">
        	<label class="uk-form-label">Nome torneo</label>
		    <div class="uk-form-controls">
		    <input type="text" name="torneo" placeholder="Nome">
		    </div>
        </div>
        <div class="uk-form-row">
        	<label class="uk-form-label">Scadenza iscrizioni</label>
        	<div class="uk-form-controls">
        	<input type="text" data-uk-datepicker="{format:'YYYY-MM-DD'}" name="scadenza" placeholder="Scadenza iscrizioni">
        	</div>
        </div>
        <div class="uk-form-row">
        	<label class="uk-form-label">Quota</label>
        	<div class="uk-form-controls">
        	<input type="number" name="quota" placeholder="Quota">
        	</div>
        </div>
        <div class="uk-form-row">
        	<label class="uk-form-label">Tipologia</label>
        	<div class="uk-form-controls">
		    <select class="tipologia" name="tipologia">
		        <option value="1">Italiana</option>
		        <option value="2">Eliminazione</option>
		        <option value="3">Misto</option>
		        <option value="4">Libero</option>
		    </select>
		    </div>
        </div>
        <div class="uk-form-row">
        	<label class="uk-form-label">Partecipanti minimi</label>
        	<div class="uk-form-controls">
        	<input type="number" name="min_partecipanti" placeholder="Partecipanti minimi">
        	</div>
        </div>
        <div class="uk-form-row">
        	<label class="uk-form-label">Partecipanti massimi</label>
        	<div class="uk-form-controls">
        	<input type="number" name="max_partecipanti" placeholder="Partecipanti massimi">
        	</div>
        </div>
        <div class="uk-form-row">
        	<label class="uk-form-label">Nome premio</label>
        	<div class="uk-form-controls">
        	<input type="text" name="premio" placeholder="Nome premio">
        	</div>
        </div>
        <div class="uk-form-row">
        	<label class="uk-form-label">Riedizione del torneo</label>
        	<div class="uk-form-controls">
			<select name="riedizione">
			<option value="">Nessuno</option>
<?php
	$tornei=$conn->query('SELECT DISTINCT T.idTorneo, nome ' . 
	'FROM Torneo AS T JOIN Ha_vinto AS H ON T.idTorneo = H.idTorneo AND nome IS NOT NULL')->fetchAll(PDO::FETCH_ASSOC);
	foreach($tornei as $row) {
		print('
			<option value="' . $row['idTorneo'] . '">' . $row['nome'] . '</option>
			');
	}
?>
        	</select>
        	</div>
        </div>
        <div id="libero" class="uk-form-row uk-hidden">
        	<label class="uk-form-label">Numero gare</label>
        	<div class="uk-form-controls">
        	<input type="number" name="num_gare" placeholder="Numero gare">
        	</div>
        </div>
        <div id="misto" class="uk-form-row uk-hidden">
        	<label class="uk-form-label">Numero gironi</label>
        	<div class="uk-form-controls">
        	<input type="number" name="num_gironi" placeholder="Numero gironi">
        	</div>
        </div>
        <button class="uk-button">Crea torneo</button>
    </fieldset>
</form>
<script>
$(".tipologia").change(function() {
	var sel = $('.tipologia').find(":selected").val();
	if(!$('#misto').hasClass('uk-hidden'))
		$('#misto').addClass('uk-hidden');
	if(!$('#libero').hasClass('uk-hidden'))
		$('#libero').addClass('uk-hidden');
	if(sel == "3") {
		if($('#misto').hasClass('uk-hidden'))
			$('#misto').removeClass('uk-hidden');
	} else if(sel == "4") {
		if($('#libero').hasClass('uk-hidden'))
			$('#libero').removeClass('uk-hidden');
	}
});
</script>
</body>

</html>
