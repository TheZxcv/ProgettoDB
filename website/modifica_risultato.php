<?php
session_start();
require_once('lib/functions.php');
$conn = open_connection();
$logged = null;
$user = null;
if(isset($_SESSION['is_logged'])){
    $logged = $_SESSION['is_logged'];
    $user = $_SESSION['user'];
}

if($logged){
    $_SESSION['is_logged'] = $logged;
    $_SESSION['user'] = $user;
}

if(!canBrowseWebsite($conn, $user)) {
	header('HTTP/1.0 403 Forbidden');
	header('Location: /redirect.php');
	return;
}

if(!isAdmin($conn, $user)) {
	$stmt = $conn->prepare('SELECT idUtente FROM Utente WHERE user = ? '.
		' AND (idUtente IN (SELECT idGiocatore '.
		' FROM Partecipa_a WHERE idTorneo = ?) '.
		' OR idUtente IN (SELECT idOrganizzatore FROM Torneo WHERE idTorneo=?))'
	);
	$stmt->bindParam(1, $user, PDO::PARAM_STR, 20);
	$stmt->bindParam(2, $_GET['torneo'], PDO::PARAM_STR, 20);
	$stmt->bindParam(3, $_GET['torneo'], PDO::PARAM_STR, 20);
	$stmt->execute();
	$count = $stmt->rowCount();
	$stmt->closeCursor();

	if($count == 0) {
		header('HTTP/1.0 403 Forbidden');
		header('Location: /redirect.php');
		return;
	}
}



if(isset($_POST['update_match'])) {
	if(!update_match_scores($conn, $_POST['torneo'], $_POST['gara'],
			$_POST['gioc'], $_POST['risul'], $_POST['punt']))
		echo "Aggiornamento dei risultati invalido.";
	unset($_POST);
}
?>
<!DOCTYPE html>
<html>

<head>
<?php include_once('lib/header.php');?>
<link href="css/comments.css" rel="stylesheet" type="text/css">
</head>

<body>
<?php
include('lib/menu.php');
// Fetching match's data
$stmt = $conn->prepare('SELECT data FROM Gara WHERE idGara=?');
$stmt->bindParam(1, $_GET['gara'], PDO::PARAM_STR, 20);
$stmt->execute();
$temp = $stmt->fetch(PDO::FETCH_ASSOC);
$data = $temp['data'];
$stmt->closeCursor();
unset($temp);
print('<h2>Informazioni sulla gara ' . $_GET['gara'] . ' [' . $data . ']</h2>');
?>

<form method="POST" action="<?php echo($_SERVER['PHP_SELF']); ?>">
<div class="uk-form-row">
<table class="uk-table uk-table-striped uk-table-hover">
	<thead>
		<tr>
			<th>Giocatore</th>
			<th>Punteggio</th>
			<th>Risultato</th>
		</tr>
	</thead>
	<tbody>
<?php
	$stmt = $conn->prepare('SELECT user,punteggio,risultato ' .
				'FROM Partecipa_a JOIN Utente ON idGiocatore=idUtente ' .
				'WHERE idTorneo=? AND idGara=? ' .
				'ORDER BY punteggio DESC, user');
	$stmt->bindParam(1, $_GET['torneo'], PDO::PARAM_STR, 20);
	$stmt->bindParam(2, $_GET['gara'], PDO::PARAM_STR, 20);
	$stmt->execute();
	$tornei = $stmt->fetchAll(PDO::FETCH_ASSOC);
	$hiddenInputs = '';
	foreach($tornei as $row) {
		print('<tr>' . PHP_EOL);
		print('<td>' . $row['user'] . '</td>' . PHP_EOL);
		print('<td><input name="punt[]" type="number" ');
		if(isset($row['punteggio']))
			print('value ="' . $row['punteggio'] . '"');
		print(' placeholder="Punteggio"></td>' . PHP_EOL);

		print('<td><input name="risul[]" type="number" ');
		if(isset($row['risultato']))
			print('value ="' . $row['risultato'] . '"');
		print(' placeholder="Risultato"></td>' . PHP_EOL);
		print('</tr>' . PHP_EOL);
		$hiddenInputs .= '<input type="hidden" name="gioc[]" value="' . $row['user'] . '"/>' . PHP_EOL;
	}
	$stmt->closeCursor();
?>
	</tbody>
</table>
</div>

<div id='submit_button'>
<?php print($hiddenInputs); ?>
	<input type="submit" value='Conferma'/>
	<input type='hidden' name='update_match' id='update_match' value='1'/>
	<input type='hidden' name='torneo' id='torneo' value='<?php echo $_GET['torneo'];?>'/>
	<input type='hidden' name='gara' id='gara' value='<?php echo $_GET['gara'];?>'/>
</div>

<a class="uk-button" href="./gara.php?torneo=<?php echo $_GET['torneo']?>&amp;gara=<?php echo $_GET['gara']?>">Indietro</a>

</form>

<?php unset($conn);?>
</body>
</html>
