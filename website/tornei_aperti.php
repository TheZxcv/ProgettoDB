<?php
session_start();
require_once('lib/functions.php');
$conn = open_connection();
$logged = null;
$user = null;
if(isset($_SESSION['is_logged'])){
    $logged = $_SESSION['is_logged'];
    $user = $_SESSION['user'];
}

if($logged){
    $_SESSION['is_logged'] = $logged;
    $_SESSION['user'] = $user;
}

if(!canBrowseWebsite($conn, $user)) {
	header('HTTP/1.0 403 Forbidden');
	header('Location: /redirect.php');
	die();
}

if(isset($_GET['torneo'])) {
	suscribe_to_torneo($conn, $user, $_GET['torneo']);
}
?>
<!DOCTYPE html>
<html>

<head>
<?php include_once('lib/header.php');?>
</head>

<body>
<?php include('lib/menu.php')?>
<br>
<form method="GET" action="<?php echo($_SERVER['PHP_SELF']); ?>">
<div class="uk-form-row">
	<button class="uk-button">Iscriviti</button>
</div>
<div class="uk-form-row">
<table class="uk-table uk-table-striped uk-table-hover">
	<thead>
		<tr>
			<th>Nome Torneo</th>
			<th>Struttura</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<?php
	$stmt = $conn->prepare('SELECT idTorneo, nome, struttura ' .
				'FROM Torneo ' .
				'WHERE nome IS NOT NULL AND scadenza_iscr > DATE(NOW()) AND ' .
				'idTorneo NOT IN (SELECT idTorneo FROM Iscritto_a JOIN Utente ON idGiocatore=idUtente WHERE user=?)');
	$stmt->bindParam(1, $user, PDO::PARAM_STR, 20);
	$stmt->execute();
	$tornei = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($tornei as $row) {
		print('<tr>' . PHP_EOL);
		print('<td>' . $row['nome'] . '</td>' . PHP_EOL);
		print('<td>' . getNomeStruttura($row['struttura']) . '</td>' . PHP_EOL);
		print('<td><input type="radio" name="torneo" value="' . $row['idTorneo'] . '"></input></td>' . PHP_EOL);
		print('</tr>' . PHP_EOL);
	}
	$stmt->closeCursor();
	unset($conn);
?>
</tbody>
</table>
</div>
</form>

</body>
</html>
