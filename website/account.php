<?php // Login chek
ini_set("error reporting", E_ALL);
session_start();
include_once('lib/functions.php');
$conn = open_connection();
$logged = null;
$user = null;
//Verifico l'esistenza del cookie e di $logged
if(isset($_POST['user'])){
    $myuser=trim($_POST['user']);
    $mypsw=trim($_POST['psw']);
    $logged=check_login($conn, $myuser, $mypsw);
    $user = $myuser;
    $has_logged=true;
}
if(isset($_SESSION['is_logged'])){
    $logged=$_SESSION['is_logged'];
    $user=$_SESSION['user'];
}
if($logged){
    $_SESSION['is_logged']=$logged;
    $_SESSION['user']=$user;
}
//Password change
if(isset($_POST['mod']) && $_POST['mod'] == 'change'){
	if(strcmp($_POST['psw'],$_POST['psw_chk']) == 0 ){ // Cambia la pass
		if($stmt = $conn->prepare("UPDATE Utente SET password=SHA2(?, 256) WHERE user = ?")){
			$stmt->bindParam(1, $_POST['psw'], PDO::PARAM_STR, 256);
			$stmt->bindParam(2, $user, PDO::PARAM_STR, 20);
			$stmt->execute();
			$changed = true;
		} 
	} else {
		$change_failed = true; // Password diverse
	}
}
//Logout
if( (isset($_POST['mod']) && $_POST['mod'] == 'out' ) || (isset($_GET['mod']) && $_GET['mod'] == 'out') ){
    unset($_SESSION['is_logged']);
    unset($_SESSION['user']);
    $logged=null;
    unset($_SESSION['mod']);
    $logout = true;
}
?>
<!DOCTYPE html>
<html>

<head>
<?php include_once('lib/header.php');?>
</head>

<body>
<?php
ini_set("error reporting", E_ALL);
include('lib/menu.php');

if(isset($logged) && isset($user)){
	if(isset($has_logged)){
	print('<div class="uk-alert uk-alert-success">Login effettuato</div>');
	unset($has_logged);
	}
	if(isset($changed)){
	print('<div class="uk-alert uk-alert-success">Password cambiata con successo</div>');
	unset($changed);
	}
	if(isset($change_failed)){
	print('<div class="uk-alert uk-alert-warning">Le due password sono diverse</div>');
	unset($change_failed);
	}
	$type = getUserType($conn, $user);
	$type_string = getTipologiaUtente($type);
	print('Sei loggato come: ' . $user . ' (tipologia: ' . $type_string . ')' . PHP_EOL);
	print('
<form class="uk-form" method="POST" action="' .  $_SERVER['PHP_SELF'] .'">
    <fieldset data-uk-margin>
        <input type="hidden" name="mod" value="out">
        <button class="uk-button">Logout</button>
    </fieldset>
</form>
<form class="uk-form" method="POST" action="' .  $_SERVER['PHP_SELF'] .'">
    <fieldset data-uk-margin>
        <legend>Cambio password</legend>
        <div class="uk-form-row">
        <input type="password" name="psw" placeholder="Password">
        </div>
        <div class="uk-form-row">
        <input type="password" name="psw_chk" placeholder="Password">
        </div>
        <input type="hidden" name="mod" value="change">
        <button class="uk-button">Cambio password</button>
    </fieldset>
</form>
');
}
else {
	if(isset($logout)){
		print('<div class="uk-alert uk-alert-success">Logout effettuato con successo</div>');
		unset($logout);
	}
	print('
<form class="uk-form" method="POST" action="' .  $_SERVER['PHP_SELF'] .'">
    <fieldset data-uk-margin>
        <legend>Inserire Username e Password</legend>
        <div class="uk-form-row">
        <input type="text" name="user" placeholder="Username">
        </div>
        <div class="uk-form-row">
        <input type="password" name="psw" placeholder="Password">
        </div>
        <button class="uk-button">Login</button>
    </fieldset>
</form>
');
}
unset($conn);
?>

</body>

</html>
