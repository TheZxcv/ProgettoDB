<?php
/** Adjust location to the one you wish
* If the webapp runs at HOSTNAME/webapp, location should be '/webapp/index.php'
* and redirect.php should be avaible at HOSTNAME/redirect.php
*/
header('Location: /index.php');
die();
?>
