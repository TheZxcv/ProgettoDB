<?php
session_start();
require_once('lib/functions.php');
$conn = open_connection();
$logged = null;
$user = null;
if(isset($_SESSION['is_logged'])){
    $logged = $_SESSION['is_logged'];
    $user = $_SESSION['user'];
}

if($logged){
    $_SESSION['is_logged'] = $logged;
    $_SESSION['user'] = $user;
}

if(!canCreateTournaments($conn, $user)) {
	header('HTTP/1.0 403 Forbidden');
	header('Location: /redirect.php');
	die();
}

if(isset($_GET['operazione']) && isset($_GET['gioc']) && isset($_GET['torneo'])) {
	if($_GET['operazione'] == 'accept') {
		subscribe_tournament($conn, $_GET['gioc'], $_GET['torneo']);
	} else if($_GET['operazione'] == 'refuse') {
		unsubscribe_tournament($conn, $_GET['gioc'], $_GET['torneo']);
	}
}
?>
<!DOCTYPE html>
<html>

<head>
<?php include_once('lib/header.php');?>
</head>

<body>
<?php include('lib/menu.php')?>
<h4>Selezionare il torneo per accedere alla pagina delle iscrizioni ad esso associate</h4>
<div class="uk-form-row">
<table class="uk-table uk-table-striped uk-table-hover">
	<thead>
		<tr>
			<th>Nome Torneo</th>
		</tr>
	</thead>
	<tbody>
<?php
	$user_id = getUserID($conn, $user);
	$stmt = $conn->prepare('SELECT idTorneo,nome FROM Torneo WHERE nome IS NOT NULL AND idOrganizzatore=?');
	$stmt->bindParam(1, $user_id, PDO::PARAM_STR, 20);
	$stmt->execute();
	$tornei = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($tornei as $row) {
		print('<tr onclick="document.location = \'modifica_iscrizioni_torneo.php?torneo=' . $row['idTorneo'] . '\';">' . PHP_EOL);
		print('<td>' . $row['nome'] . '</td>' . PHP_EOL);
		print('</tr>' . PHP_EOL);
	}
	$stmt->closeCursor();
	unset($conn);
?>
	</tbody>
</table>
</div>
</body>
</html>
