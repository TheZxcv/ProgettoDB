<?php
session_start();
require_once('lib/functions.php');
$conn = open_connection();
$logged = null;
$user = null;
if(isset($_SESSION['is_logged'])){
    $logged = $_SESSION['is_logged'];
    $user = $_SESSION['user'];
}

if($logged){
    $_SESSION['is_logged'] = $logged;
    $_SESSION['user'] = $user;
}

if(!canBrowseWebsite($conn, $user)) {
	header('HTTP/1.0 403 Forbidden');
	header('Location: /redirect.php');
	die();
}
?>
<!DOCTYPE html>
<html>

<head>
<?php include_once('lib/header.php');?>
</head>

<body>
<?php include('lib/menu.php')?>
<h3>Storico Tornei</h3>
<h4>Cliccare sulla voce in tabella per selezionare il girone specifico</h4>
<div class="uk-form-row">
<table class="uk-table uk-table-striped uk-table-hover">
	<thead>
		<tr>
			<th>Girone</th>
			<th>Struttura</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<?php
	// I gironi all'italiana
	$stmt = $conn->prepare('SELECT idTorneo_allitaliana as idTorneo, girone ' .
				'FROM Possiede_girone ' .
				'WHERE idTorneo_misto=?');
	$stmt->bindParam(1, $_GET['torneo'], PDO::PARAM_STR, 20);
	$stmt->execute();
	$tornei = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($tornei as $row) {
		print('<tr onclick="document.location = \'lista_gare.php?torneo=' . $row['idTorneo'] . '\';">' . PHP_EOL);
		print('<td>' . $row['girone'] . '</td>' . PHP_EOL);
		print('<td>' . getNomeStruttura(1) . '</td>' . PHP_EOL);
		print('<td><a href="classifica.php?torneo=' . $row['idTorneo'] . '">Classifica</a></td>' . PHP_EOL);
		print('</tr>' . PHP_EOL);
	}
	$stmt->closeCursor();

	// Il torneo ad eliminazione
	$stmt = $conn->prepare('SELECT idTorneo_Eliminazione AS idTorneo ' .
				'FROM Torneo_Misto ' .
				'WHERE idTorneo=?');
	$stmt->bindParam(1, $_GET['torneo'], PDO::PARAM_STR, 20);
	$stmt->execute();
	$torneo = $stmt->fetch(PDO::FETCH_ASSOC);
	if(isset($torneo['idTorneo'])){
		print('<tr onclick="document.location = \'lista_gare.php?torneo=' . $torneo['idTorneo'] . '\';">' . PHP_EOL);
		print('<td><h4>Torneo Finale</h4></td>' . PHP_EOL);
		print('<td>' . getNomeStruttura(2) . '</td>' . PHP_EOL);
		print('<td><a href="classifica.php?torneo=' . $torneo['idTorneo'] . '">Classifica</a></td>' . PHP_EOL);
		print('</tr>' . PHP_EOL);
	} else {
		print('<tr>' . PHP_EOL);
		print('<td>Torneo finale (da definirsi)</td>' . PHP_EOL);
		print('<td></td>' . PHP_EOL);
		print('<td></td>' . PHP_EOL);
		print('</tr>' . PHP_EOL);
	}
	$stmt->closeCursor();
	unset($conn);
?>
	</tbody>
</table>
</div>

</body>
</html>
