<?php
session_start();
require_once('lib/functions.php');
$conn = open_connection();
$logged = null;
$user = null;
if(isset($_SESSION['is_logged'])){
    $logged = $_SESSION['is_logged'];
    $user = $_SESSION['user'];
}

if($logged){
    $_SESSION['is_logged'] = $logged;
    $_SESSION['user'] = $user;
}

if(!canBrowseWebsite($conn, $user)) {
	header('HTTP/1.0 403 Forbidden');
	header('Location: /redirect.php');
	die();
}
?>
<!DOCTYPE html>
<html>

<head>
<?php include_once('lib/header.php');?>
</head>

<body>
<?php include('lib/menu.php')?>
<h3>Storico Tornei</h3>
<h4>Cliccare sulla voce in tabella per visualizzare le gare del torneo scelto</h4>
<div class="uk-form-row">
<table class="uk-table uk-table-striped uk-table-hover">
	<thead>
		<tr>
			<th>Nome Torneo</th>
			<th>Struttura</th>
			<th>Aperto?</th>
			<th></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<?php
	$admin = isAdmin($conn, $user);
	$stmt = $conn->prepare('SELECT idTorneo, nome, struttura, now() <= scadenza_iscr as aperto ' .
				'FROM Torneo ' .
				'WHERE nome IS NOT NULL ORDER BY aperto DESC, idTorneo ASC');
	$stmt->execute();
	$tornei = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($tornei as $row) {
		if($row['struttura'] == 3)
			print('<tr onclick="document.location = \'lista_gironi.php?torneo=' . $row['idTorneo'] . '\';">' . PHP_EOL);
		else
			print('<tr onclick="document.location = \'lista_gare.php?torneo=' . $row['idTorneo'] . '\';">' . PHP_EOL);
		print('<td>' . $row['nome'] . '</td>' . PHP_EOL);
		print('<td>' . getNomeStruttura($row['struttura']) . '</td>' . PHP_EOL);
		if($row['aperto'] == 1)
			print('<td> Sì </td>' . PHP_EOL);
		else
			print('<td> No </td>' . PHP_EOL);
		if($row['struttura'] == 3)
			print('<td>Vedere la classifica dei gironi</td>' . PHP_EOL);
		else
			print('<td><a href="classifica.php?torneo=' . $row['idTorneo'] . '">Classifica</a></td>' . PHP_EOL);
		if($admin)
			print('<td><a href="modifica_torneo.php?torneo=' . $row['idTorneo'] . '">Modifica</a></td>' . PHP_EOL);
		else
			print('<td></td>' . PHP_EOL);
		print('</tr>' . PHP_EOL);
	}
	$stmt->closeCursor();
	unset($conn);
?>
	</tbody>
</table>
</div>

</body>
</html>
