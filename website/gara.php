<?php
session_start();
require_once('lib/functions.php');
$conn = open_connection();
$logged = null;
$user = null;
if(isset($_SESSION['is_logged'])){
    $logged = $_SESSION['is_logged'];
    $user = $_SESSION['user'];
}

if($logged){
    $_SESSION['is_logged'] = $logged;
    $_SESSION['user'] = $user;
}

if(!canBrowseWebsite($conn, $user)) {
	header('HTTP/1.0 403 Forbidden');
	header('Location: /redirect.php');
	die();
}


if(isset($_POST['send_comment'])){
	$stmt = $conn->prepare("CALL aggiungi_commento(?,?,?,?,?)");
	$stmt->bindParam(1, $user, PDO::PARAM_STR, 20);
	$stmt->bindParam(2, $_GET['torneo'], PDO::PARAM_INT);
	$stmt->bindParam(3, $_GET['gara'], PDO::PARAM_INT);
	$stmt->bindParam(4, $_POST['comment_body'], PDO::PARAM_STR, 255);
	$parent_id = is_numeric($_POST['parent_id']) ? $_POST['parent_id'] : null;
	$stmt->bindParam(5, $parent_id, PDO::PARAM_INT);

	if(!$stmt->execute()) {
		echo "Comment cannot be posted. Please try again.";
	}
	unset($_POST);
}

?>
<!DOCTYPE html>
<html>

<head>
<?php include_once('lib/header.php');?>
<link href="css/comments.css" rel="stylesheet" type="text/css">
<script type='text/javascript'>
$(function(){
 $("a.reply").click(function() {
  var id = $(this).attr("id");
  $("#parent_id").attr("value", id);
 });
});
</script>
</head>

<body>
<?php
include('lib/menu.php');
// Fetching match's data
$stmt = $conn->prepare('SELECT data FROM Gara WHERE idGara=?');
$stmt->bindParam(1, $_GET['gara'], PDO::PARAM_STR, 20);
$stmt->execute();
$temp = $stmt->fetch(PDO::FETCH_ASSOC);
$data = $temp['data'];
$stmt->closeCursor();
unset($temp);
print('<h2>Informazioni sulla gara ' . $_GET['gara'] . ' [' . $data . ']</h2>');
?>
<form method="GET" action="<?php echo($_SERVER['PHP_SELF']); ?>">
<div class="uk-form-row">
<table class="uk-table uk-table-striped uk-table-hover">
	<thead>
		<tr>
			<th>Giocatore</th>
			<th>Punteggio</th>
			<th>Risultato</th>
		</tr>
	</thead>
	<tbody>
<?php
	$stmt = $conn->prepare('SELECT user,punteggio,risultato ' .
				'FROM Partecipa_a JOIN Utente ON idGiocatore=idUtente ' .
				'WHERE idTorneo=? AND idGara=? ' .
				'ORDER BY punteggio DESC, user');
	$stmt->bindParam(1, $_GET['torneo'], PDO::PARAM_STR, 20);
	$stmt->bindParam(2, $_GET['gara'], PDO::PARAM_STR, 20);
	$stmt->execute();
	$tornei = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($tornei as $row) {
		print('<tr>' . PHP_EOL);
		print('<td>' . $row['user'] . '</td>' . PHP_EOL);
		if(isset($row['punteggio']))
			print('<td>' . $row['punteggio'] . '</td>' . PHP_EOL);
		else
			print('<td>Da disputare</td>' . PHP_EOL);
		if(isset($row['risultato']))
			print('<td>' . $row['risultato'] . '</td>' . PHP_EOL);
		else
			print('<td>Da disputare</td>' . PHP_EOL);
		print('</tr>' . PHP_EOL);
	}
	$stmt->closeCursor();
?>
	</tbody>
</table>
</div>
</form>

<ul class='comment-tree'>
<?php
	$stmt = $conn->prepare("SELECT * FROM Commento WHERE idGara = ? AND idTorneo = ? AND idCommento_parent IS NULL");
	$stmt->bindParam(1, $_GET['gara'], PDO::PARAM_STR, 20);
	$stmt->bindParam(2, $_GET['torneo'], PDO::PARAM_STR, 20);
	$stmt->execute();
	if($stmt->rowCount() > 0){
		$commenti = $stmt->fetchAll(PDO::FETCH_ASSOC);
		foreach($commenti as $row) 
			getComments($conn, $row);
	}
	$stmt->closeCursor();
?>
</ul>

<a class="uk-button" href="./modifica_risultato.php?torneo=<?php echo $_GET['torneo']?>&amp;gara=<?php echo $_GET['gara']?>">Modifica risultati</a>
<form id="comment_form" action="<?php echo($_SERVER['PHP_SELF']); ?>?torneo=<?php echo $_GET['torneo']?>&amp;gara=<?php echo $_GET['gara']?>" method="POST">
	<label>Nome: <?php echo(getUserFullName($conn, getUserId($conn, $user))); ?></label>
	<br>
	<label for="comment_body">Commento:</label>
	<textarea name="comment_body" id='comment_body'></textarea>
	<input type='hidden' name='parent_id' id='parent_id' value='NULL'/>
	<div id='submit_button'>
		<input type="submit" value="Aggiungi commento"/>
		<input type='hidden' name='send_comment' id='send_comment' value='1'/>
	</div>
</form>

<?php unset($conn);?>
</body>
</html>
