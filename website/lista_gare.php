<?php
session_start();
require_once('lib/functions.php');
$conn = open_connection();
$logged = null;
$user = null;
if(isset($_SESSION['is_logged'])){
    $logged = $_SESSION['is_logged'];
    $user = $_SESSION['user'];
}

if($logged){
    $_SESSION['is_logged'] = $logged;
    $_SESSION['user'] = $user;
}

if(!canBrowseWebsite($conn, $user)) {
	header('HTTP/1.0 403 Forbidden');
	header('Location: /redirect.php');
	die();
}
?>
<!DOCTYPE html>
<html>

<head>
<?php include_once('lib/header.php');?>
</head>

<body>
<?php include('lib/menu.php')?>
<h3>Cliccare sulla voce in tabella per selezionare la gara specifica</h3>
<form method="GET" action="<?php echo($_SERVER['PHP_SELF']); ?>">
<div class="uk-form-row">
<table class="uk-table uk-table-striped uk-table-hover">
	<thead>
		<tr>
			<th>Numero gara</th>
			<th>Data</th>
			<th>Fase</th>
		</tr>
	</thead>
	<tbody>
<?php
	$stmt = $conn->prepare('SELECT * FROM (SELECT @counter:=@counter+1 AS num, idGara,data,fase ' .
				'FROM Gara JOIN (SELECT @counter := 0) AS gare ' .
				'WHERE idTorneo=? ' .
				'ORDER BY fase ASC, data ASC) AS tabella ORDER BY num DESC');
	$stmt->bindParam(1, $_GET['torneo'], PDO::PARAM_STR, 20);
	$stmt->execute();
	$tornei = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($tornei as $row) {
		print('<tr onclick="document.location = \'gara.php?torneo=' . $_GET['torneo'] . '&amp;gara=' . $row['idGara'] . '\';">' . PHP_EOL);
		print('<td>' . $row['num'] . '</td>' . PHP_EOL);
		print('<td>' . $row['data'] . '</td>' . PHP_EOL);
		if(isset($row['fase']))
			print('<td>' . $row['fase'] . '</td>' . PHP_EOL);
		else
			print('<td>Nessuna</td>' . PHP_EOL);
		print('</tr>' . PHP_EOL);
	}
	$stmt->closeCursor();
	unset($conn);
?>
	</tbody>
</table>
</div>
</form>

</body>
</html>
