<?php
	include_once('lib/functions.php');
	if(isset($_POST['reg_user'])) {
		if(strcmp($_POST['reg_psw'], $_POST['reg_psw_chk']) == 0) {
			$error_psw = false;
			$conn = open_connection();
			if(check_free_username($conn, $_POST['reg_user'])) {
				$success_signup = sign_up_player($conn, $_POST['reg_user'], $_POST['reg_psw'], $_POST['reg_name'], $_POST['reg_surname']);
			} else
				$success_signup = false;
			unset($conn);
		} else {
			$error_psw = true;
			$success_signup = false;
		}
	}
	unset($_POST);
?>
<!DOCTYPE html>
<html>

<head>
<?php include_once('lib/header.php');?>
</head>

<body>
<?php
include('lib/menu.php');
if(isset($error_psw) && isset($success_signup)) {
		if($error_psw)
			print('<div class="uk-alert uk-alert-warning">Le password sono diverse</div>');
		else if($success_signup)
			print('<div class="uk-alert uk-alert-success">Registrazione avvenuta con successo</div>');
		else if(isset($success_signup))
			print('<div class="uk-alert uk-alert-danger">Registrazione fallita (nome utente non disponibile)</div>');
		print('<a class="uk-button" href="">Ricarica</a>');
} else print('
<form class="uk-form" method="POST" action="' . $_SERVER['PHP_SELF'] . '">
    <fieldset data-uk-margin>
	<legend>Inserire dati utente</legend>
	<div class="uk-form-row">
        <input type="text" name="reg_user" placeholder="Username">
    </div>
    <div class="uk-form-row">
		<input type="password" name="reg_psw" placeholder="Password" maxlength="256">
	</div>
	<div class="uk-form-row">
		<input type="password" name="reg_psw_chk" placeholder="Password" maxlength="256">
	</div>
	<div class="uk-form-row">
        <input type="text" name="reg_name" placeholder="Nome">
	</div>
	<div class="uk-form-row">
		<input type="text" name="reg_surname" placeholder="Cognome">
	</div>
    <button class="uk-button">Registrati</button>
    </fieldset>
</form>
');
?>
</body>
</html>
