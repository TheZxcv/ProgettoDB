<?php
session_start();
require_once('lib/functions.php');
$conn = open_connection();
$logged = null;
$user = null;
if(isset($_SESSION['is_logged'])){
    $logged = $_SESSION['is_logged'];
    $user = $_SESSION['user'];
}

if($logged){
    $_SESSION['is_logged'] = $logged;
    $_SESSION['user'] = $user;
}

if(!canBrowseWebsite($conn, $user)) {
	header('HTTP/1.0 403 Forbidden');
	header('Location: /redirect.php');
	die();
}

?>
<!DOCTYPE html>
<html>

<head>
<?php include_once('lib/header.php');?>
</head>

<body>
<?php
include('lib/menu.php');
print("<h2>Classifica del torneo " . $_GET['torneo'] . "</h2>");
?>

<form>
<div class="uk-form-row">
<table class="uk-table uk-table-striped uk-table-hover">
	<thead>
		<tr>
			<th>Giocatore</th>
			<th>Punteggio</th>
		</tr>
	</thead>
	<tbody>
<?php
	$stmt = $conn->prepare('SELECT user,punti ' .
				'FROM Classifica JOIN Utente ON idGiocatore=idUtente ' .
				'WHERE idTorneo=? ' .
				'ORDER BY punti DESC, user');
	$stmt->bindParam(1, $_GET['torneo'], PDO::PARAM_STR, 20);
	$stmt->execute();
	$tornei = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($tornei as $row) {
		print('<tr>' . PHP_EOL);
		print('<td>' . $row['user'] . '</td>' . PHP_EOL);
		print('<td>' . $row['punti'] . '</td>' . PHP_EOL);
		print('</tr>' . PHP_EOL);
	}
	$stmt->closeCursor();
	unset($conn);
?>
	</tbody>
</table>
</div>
</form>

</body>
</html>
