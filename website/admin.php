<?php
session_start();
require_once('lib/functions.php');
$conn = open_connection();
$logged = null;
$user = null;
if(isset($_SESSION['is_logged'])){
    $logged = $_SESSION['is_logged'];
    $user = $_SESSION['user'];
}

if($logged){
    $_SESSION['is_logged'] = $logged;
    $_SESSION['user'] = $user;
}

if(isset($_GET['operazione']) && isset($_GET['username'])) {
	if($_GET['operazione'] == 'manager') {
		make_manager($conn, $_GET['username']);
	} else if($_GET['operazione'] == 'unmanager') {
		remove_manager($conn, $_GET['username']);
	} else if($_GET['operazione'] == 'admin') {
		make_admin($conn, $_GET['username']);
	} else if($_GET['operazione'] == 'unadmin') {
		remove_admin($conn, $_GET['username']);
	} else if($_GET['operazione'] == 'ban') {
		ban_user($conn, $_GET['username']);
	} else if($_GET['operazione'] == 'unban') {
		unban_user($conn, $_GET['username']);
	}
}

if(!isAdmin($conn, $user)) {
	header('HTTP/1.0 403 Forbidden');
	header('Location: /redirect.php');
	die();
}
?>
<!DOCTYPE html>
<html>

<head>
<?php include_once('lib/header.php');?>
</head>

<body>
<?php include('lib/menu.php')?>
<form method="GET" action="<?php echo($_SERVER['PHP_SELF']); ?>">
<br>
<div class="uk-form-row">
	<select name="operazione">
		<option value="manager">Rendi organizzatore</option>
		<option value="unmanager">Rimuovi organizzatore</option>
		<option value="admin">Rendi amministratore</option>
		<option value="unadmin">Rimuovi amministratore</option>
		<option value="ban">Blocca accesso</option>
		<option value="unban">Sblocca accesso</option>
	</select>
</div>
<div class="uk-form-row">
	<button class="uk-button">Esegui</button>
</div>
<div class="uk-form-row">
<table class="uk-table uk-table-striped uk-table-hover">
	<thead>
		<tr>
			<th>ID Utente</th>
			<th>Nome Utente</th>
			<th>Tipologia</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<?php
	$users = $conn->query('SELECT idUtente, user FROM Utente')->fetchAll(PDO::FETCH_ASSOC);
	foreach($users as $row) {
		print('<tr>' . PHP_EOL);
		print('<td>' . $row['idUtente'] . '</td>' . PHP_EOL);
		print('<td>' . $row['user'] . '</td>' . PHP_EOL);
		$type = getUserType($conn, $row['user']);
		$type_string = getTipologiaUtente($type);
		print('<td>' . $type_string . '</td>' . PHP_EOL);
		print('<td><input type="radio" name="username" value="' . $row['user'] . '"></td>' . PHP_EOL);
		print('</tr>' . PHP_EOL);
	}
	unset($conn);
?>
	</tbody>
</table>
</div>
</form>

</body>
</html>
