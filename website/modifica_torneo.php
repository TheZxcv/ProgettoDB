<?php // Login chek
ini_set("error reporting", E_ALL);
session_start();
include_once('lib/functions.php');
$conn = open_connection();
$logged = null;
$user = null;
//Verifico l'esistenza del cookie e di $logged
if(isset($_POST['user'])){
    $myuser=trim($_POST['user']);
    $mypsw=trim($_POST['psw']);
    $logged=check_login($conn, $myuser, $mypsw);
    $user = $myuser;
}
if(isset($_SESSION['is_logged'])){
    $logged=$_SESSION['is_logged'];
    $user=$_SESSION['user'];
}
if($logged){
    $_SESSION['is_logged']=$logged;
    $_SESSION['user']=$user;
}

if(!isAdmin($conn, $user)) {
	header('HTTP/1.0 403 Forbidden');
	header('Location: /redirect.php');
	die();
}

if(isset($_POST['mod']) && $_POST['mod'] == 'out'){
    unset($_SESSION['is_logged']);
    unset($_SESSION['user']);
    $logged=null;
    unset($_SESSION['mod']);
}

if(isset($_POST['torneo'])){
	if($_POST['riedizione']=='') $_POST['riedizione'] = NULL;
	modify_torneo($conn, $user, $_POST['torneo'], $_POST['nome_torneo'], $_POST['scadenza'],
			$_POST['quota'], $_POST['premio'], $_POST['riedizione']);
	
}
?>
<!DOCTYPE html>
<html>

<head>
<?php include_once('lib/header.php');?>
	<link href="uikit/css/components/datepicker.css" rel="stylesheet" type="text/css">
	<script src="uikit/js/components/datepicker.min.js"></script>
</head>

<body>
<?php
ini_set("error reporting", E_ALL);
include('lib/menu.php');

// Getting values
$stmt = $conn->prepare('SELECT nome,scadenza_iscr AS scadenza,quota_iscr AS quota,premio,idTorneo_precedente ' .
				'FROM Torneo ' .
				'WHERE idTorneo=?');
$stmt->bindParam(1, $_GET['torneo'], PDO::PARAM_STR, 20);
$stmt->execute();
$torneo = $stmt->fetch(PDO::FETCH_ASSOC);
?>

<form class="uk-form uk-form-horizontal" method="POST" action="<?php echo($_SERVER['PHP_SELF']);?>">
    <fieldset data-uk-margin>
        <legend>Inserire i dati per creare il torneo</legend>
        <div class="uk-form-row">
        	<label class="uk-form-label">Nome torneo</label>
		    <div class="uk-form-controls">
		    <input type="text" name="nome_torneo" placeholder="Nome" value="<?php echo($torneo['nome']);?>">
		    </div>
        </div>
        <div class="uk-form-row">
        	<label class="uk-form-label">Scadenza iscrizioni</label>
        	<div class="uk-form-controls">
        	<input type="text" data-uk-datepicker="{format:'YYYY-MM-DD'}" name="scadenza" placeholder="Scadenza iscrizioni" value="<?php echo($torneo['scadenza']);?>">
        	</div>
        </div>
        <div class="uk-form-row">
        	<label class="uk-form-label">Quota</label>
        	<div class="uk-form-controls">
        	<input type="number" name="quota" placeholder="Quota" value="<?php echo($torneo['quota']);?>">
        	</div>
        </div>
        <div class="uk-form-row">
        	<label class="uk-form-label">Nome premio</label>
        	<div class="uk-form-controls">
        	<input type="text" name="premio" placeholder="Nome premio" value="<?php echo($torneo['premio']);?>">
        	</div>
        </div>
        <div class="uk-form-row">
        	<label class="uk-form-label">Riedizione del torneo</label>
        	<div class="uk-form-controls">
			<select name="riedizione">
			<?php // So beautiful
			$stmt = $conn->prepare('SELECT nome FROM Torneo WHERE idTorneo=?');
			$stmt->bindParam(1, $torneo['idTorneo_precedente'], PDO::PARAM_STR, 20);
			$stmt->execute();
			$temp = $stmt->fetch(PDO::FETCH_ASSOC);
			if($torneo['idTorneo_precedente'] != null)
				echo('<option value="'. $torneo['idTorneo_precedente'] .'">' . $temp['nome'].'</option>' . PHP_EOL);
			?>
			<option value="">Nessuno</option>
<?php
	$tornei=$conn->query('SELECT DISTINCT T.idTorneo, nome ' . 
	'FROM Torneo AS T JOIN Ha_vinto AS H ON T.idTorneo = H.idTorneo AND nome IS NOT NULL')->fetchAll(PDO::FETCH_ASSOC);
	foreach($tornei as $row) {
		print('
			<option value="' . $row['idTorneo'] . '">' . $row['nome'] . '</option>
			');
	}
?>
        	</select>
        	</div>
        </div>
	<input type='hidden' name='torneo' id='torneo' value='<?php echo $_GET['torneo'];?>'/>
        <button class="uk-button">Aggiorna torneo</button>
    </fieldset>
</form>
<?php unset($conn);?>
</body>

</html>
