<?php
include_once(dirname(__FILE__) . '/../conf/conf.php');

/* bitmask perms */
define("UNKNOWN_USER",		0x00);
define("USER_NOT_LOGGED",	0x00);
define("MASK_BANNED",		0x01);
define("MASK_USER",		0x02);
define("MASK_PLAYER",		0x04);
define("MASK_ORG",		0x08);
define("MASK_ADMIN",		0x10);

define("MASK_WEB", MASK_ORG | MASK_ADMIN |
		MASK_PLAYER | MASK_USER); /* can use the website */
define("MASK_TC", MASK_ORG | MASK_ADMIN); /* can create tourn.. and match */

function open_connection(){
	$conn = new PDO('mysql:host=' . getenv("DB_ADDRESS") . ';dbname=' . getenv("DB_NAME"), getenv("DB_USER"), getenv("DB_USER_PASS"));
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $conn;
}

function sign_up_player($conn, $username, $password, $name, $surname) {
	$ret = false;
	if($stmt = $conn->prepare("CALL registra_utente(?, ?, @ret)")) {
		$stmt->bindParam(1, $username, PDO::PARAM_STR, 20);
		$stmt->bindParam(2, $password, PDO::PARAM_STR, 256);
		$stmt->execute();
		$stmt->closeCursor();

		$ret = getResultValue($conn);
		
		if($ret == true) {
			$stmt = $conn->prepare("CALL rendi_giocatore(?, ?, ?)");
			$stmt->bindParam(1, $username, PDO::PARAM_STR, 20);
			$stmt->bindParam(2, $name, PDO::PARAM_STR, 50);
			$stmt->bindParam(3, $surname, PDO::PARAM_STR, 50);
			$stmt->execute();
			$stmt->closeCursor();
		}
	} else
		print('failed');
	return $ret;
}

function getResultValue($conn) {
	$res = $conn->query("SELECT @ret AS ret")->fetch(PDO::FETCH_ASSOC);
	return $res['ret']; 
}

function check_login($conn, $username, $password) {
	if($stmt = $conn->prepare("SELECT esiste_utente(?, ?)")) {
		$stmt->bindParam(1, $username, PDO::PARAM_STR, 20);
		$stmt->bindParam(2, $password, PDO::PARAM_STR, 256);
		$stmt->execute();

		$res = $stmt->fetch(PDO::FETCH_NUM);
		$stmt->closeCursor();

		if($res[0] == true)
			return true;
		else
			return false;
	}
	return false;
}

function check_free_username($conn, $username) {
	if($stmt = $conn->prepare("select count(*) from Utente where user=?")) {
		$stmt->bindParam(1, $username, PDO::PARAM_STR, 20);
		$stmt->execute();

		$res = $stmt->fetch(PDO::FETCH_NUM);
		$stmt->closeCursor();

		if($res[0] > 0)
			return false;
		else
			return true;
	}
	return false;
}

function getUserType($conn, $user) {
	if($stmt = $conn->prepare("SELECT tipo_utente(?)")) {
		$stmt->bindParam(1, $user, PDO::PARAM_STR, 20);
		$stmt->execute();
		$perm = $stmt->fetch(PDO::FETCH_NUM)[0];
		$stmt->closeCursor();

		if($perm & MASK_BANNED)
			return 'B';
		else if($perm & MASK_ADMIN)
			return 'A';
		else if($perm & MASK_ORG)
			return 'O';
		else if($perm & MASK_PLAYER)
			return 'P';
		else if($perm & MASK_USER)
			return 'U';
		else
			return 'N';
	}
}

function getUserPermissions($conn, $user) {
	if($stmt = $conn->prepare("SELECT tipo_utente(?)")) {
		$stmt->bindParam(1, $user, PDO::PARAM_STR, 20);
		$stmt->execute();
		$perm = $stmt->fetch(PDO::FETCH_NUM)[0];
		$stmt->closeCursor();
		return $perm;
	}
	return UNKNOWN_USER;
}

function getPlayerRank($conn, $user, $idtorneo) {
	if($stmt = $conn->prepare("SELECT posizione_nel_torneo((SELECT idUtente FROM Utente WHERE user=?), ?)")) {
		$stmt->bindParam(1, $user, PDO::PARAM_STR, 20);
		$stmt->bindParam(2, $idtorneo, PDO::PARAM_INT);
		$stmt->execute();
		$res = $stmt->fetch(PDO::FETCH_NUM);
		$stmt->closeCursor();
		return $res[0];
	}
}

function getNomeStruttura($struttura) {
	switch($struttura) {
	case 1:
		return 'Italiana';
	case 2:
		return 'Eliminazione';
	case 3:
		return 'Misto';
	case 4:
		return 'Libero';
	default:
		return 'Sconosciuto';
	}
}

function suscribe_to_torneo($conn, $user, $idtorneo) {
	if($stmt = $conn->prepare("CALL iscrivi_a(?, ?, 0)")) {
		$stmt->bindParam(1, $user, PDO::PARAM_STR, 20);
		$stmt->bindParam(2, $idtorneo, PDO::PARAM_INT);
		$stmt->execute();
		$stmt->closeCursor();
	}
}

function create_torneo($conn, $user, $nometorneo, $scadenza_iscr, $quota, $struttura,
		$min_part, $max_part, $premio, $riedizione, $num_gare, $num_gironi) {
	if($stmt = $conn->prepare("CALL crea_torneo(?, ?, STR_TO_DATE(?, '%Y-%m-%d'), ?, ?, ?, ?, ?, ?, ?, ?)")) {
		$stmt->bindParam(1, $user, PDO::PARAM_STR, 20);
		$stmt->bindParam(2, $nometorneo, PDO::PARAM_STR, 45);
		$stmt->bindParam(3, $scadenza_iscr, PDO::PARAM_STR, 10);
		$stmt->bindParam(4, $quota, PDO::PARAM_STR, 7);
		$stmt->bindParam(5, $num_gare, PDO::PARAM_INT);
		$stmt->bindParam(6, $struttura, PDO::PARAM_INT);
		$stmt->bindParam(7, $min_part, PDO::PARAM_INT);
		$stmt->bindParam(8, $max_part, PDO::PARAM_INT);
		$stmt->bindParam(9, $premio, PDO::PARAM_STR, 45);
		$stmt->bindParam(10, $riedizione, PDO::PARAM_INT);
		$stmt->bindParam(11, $num_gironi, PDO::PARAM_INT);

		$stmt->execute();
		$stmt->closeCursor();
	}
}

function create_match($conn, $torneo, $data, $giocatori) {
	$conn->beginTransaction();
	try {
		$stmt = $conn->prepare('INSERT INTO Gara(idTorneo, data) VALUES(?, ?)');
		$stmt->bindParam(1, $torneo, PDO::PARAM_INT);
		$stmt->bindParam(2, $data, PDO::PARAM_STR, 20);
		$stmt->execute();
		$stmt->closeCursor();

		$gara = $conn->query('SELECT LAST_INSERT_ID()')->fetch(PDO::FETCH_NUM)[0];

		$stmt = $conn->prepare('INSERT INTO Partecipa_a(idGiocatore, idGara, idTorneo) ' .
					'VALUES (?, ?, ?)');
		foreach($giocatori as $g) {
			if(is_numeric($g)) {
				$stmt->bindParam(1, $g, PDO::PARAM_INT);
				$stmt->bindParam(2, $gara, PDO::PARAM_INT);
				$stmt->bindParam(3, $torneo, PDO::PARAM_INT);
				$stmt->execute();
			}
		}
		$stmt->closeCursor();

		$stmt = $conn->prepare('CALL check_gara(?, ?)');
		$stmt->bindParam(1, $torneo, PDO::PARAM_INT);
		$stmt->bindParam(2, $gara, PDO::PARAM_INT);
		$stmt->execute();
		$stmt->closeCursor();
	} catch (Exception $e) {
		$conn->rollBack();
		return false;
	}
	$conn->commit();
	return true;
}

function getTipologiaUtente($lettera) {
	switch($lettera) {
	case 'N':
		return 'Inesistente';
	case 'B':
		return 'Bannato';
	case 'A':
		return 'Amministratore';
	case 'U':
		return 'Utente standard';
	case 'O':
		return 'Organizzatore';
	case 'P':
		return 'Giocatore';
	default:
		return 'Sconosciuto';
	}
}

function canBrowseWebsite($conn, $user){
	$permissions = getUserPermissions($conn, $user);
	return (MASK_WEB & $permissions) > 0;
}

function canCreateTournaments($conn, $user){
	$permissions = getUserPermissions($conn, $user);
	return (MASK_TC & $permissions) > 0;
}

function isAdmin($conn, $user){
	$permissions = getUserPermissions($conn, $user);
	return (MASK_ADMIN & $permissions) > 0;
}

function make_manager($conn, $username) {
	$stmt = $conn->prepare("CALL rendi_organizzatore(?)");
	$stmt->bindParam(1, $username, PDO::PARAM_STR, 20);
	$stmt->execute();
	$stmt->closeCursor();
}

function remove_manager($conn, $username) {
	$stmt = $conn->prepare("CALL rimuovi_organizzatore(?)");
	$stmt->bindParam(1, $username, PDO::PARAM_STR, 20);
	$stmt->execute();
	$stmt->closeCursor();
}

function make_admin($conn, $username) {
	$stmt = $conn->prepare("CALL aggiungi_amministratore(?)");
	$stmt->bindParam(1, $username, PDO::PARAM_STR, 20);
	$stmt->execute();
	$stmt->closeCursor();
}

function remove_admin($conn, $username) {
	$stmt = $conn->prepare("CALL rimuovi_amministratore(?)");
	$stmt->bindParam(1, $username, PDO::PARAM_STR, 20);
	$stmt->execute();
	$stmt->closeCursor();
}

function ban_user($conn, $username) {
	$stmt = $conn->prepare("CALL ban_utente(?)");
	$stmt->bindParam(1, $username, PDO::PARAM_STR, 20);
	$stmt->execute();
	$stmt->closeCursor();
}

function unban_user($conn, $username) {
	$stmt = $conn->prepare("CALL revoca_ban_utente(?)");
	$stmt->bindParam(1, $username, PDO::PARAM_STR, 20);
	$stmt->execute();
	$stmt->closeCursor();
}

function subscribe_tournament($conn, $user, $tournament){
	$stmt = $conn->prepare("UPDATE Iscritto_a SET approvata=1 WHERE idGiocatore=? AND idTorneo=?");
	$stmt->bindParam(1, $user, PDO::PARAM_STR, 20);
	$stmt->bindParam(2, $tournament, PDO::PARAM_STR, 20);
	$stmt->execute();
	$stmt->closeCursor();
}

function unsubscribe_tournament($conn, $user, $tournament){
	try {
		$stmt = $conn->prepare("DELETE FROM Iscritto_a WHERE idGiocatore=? AND idTorneo=?");
		$stmt->bindParam(1, $user, PDO::PARAM_STR, 20);
		$stmt->bindParam(2, $tournament, PDO::PARAM_STR, 20);
		$stmt->execute();
		$stmt->closeCursor();
	} catch (Exception $e) {
		return false;
	}
	return true;
}

function getUserID($conn, $username) {
	$stmt = $conn->prepare("SELECT idUtente FROM Utente WHERE user=?");
	$stmt->bindParam(1, $username, PDO::PARAM_STR, 20);
	$stmt->execute();
	$id = $stmt->fetch(PDO::FETCH_ASSOC)['idUtente'];
	return $id;
}

function getUserName($conn, $user_id) {
	$stmt = $conn->prepare("SELECT user FROM Giocatore NATURAL JOIN Utente WHERE idUtente = ?");
	$stmt->bindParam(1, $user_id, PDO::PARAM_STR, 20);
	$stmt->execute();
	$name = $stmt->fetch(PDO::FETCH_ASSOC)['user'];
	return $name;
}

function getUserFullName($conn, $user_id) {
	$stmt = $conn->prepare("SELECT nome,cognome,user FROM Giocatore NATURAL JOIN Utente WHERE idUtente = ?");
	$stmt->bindParam(1, $user_id, PDO::PARAM_STR, 20);
	$stmt->execute();
	$temp = $stmt->fetch(PDO::FETCH_ASSOC);
	$name = $temp['nome'] . " " . $temp['cognome'] . " (" . $temp['user'] .  ")";
	return $name;
}

function getComments($conn, $row) {
	echo "<li class='comment'>";
	echo "<div class='aut'>". getUserFullName($conn, $row['idGiocatore']) ."</div>";
	echo "<div class='comment-body'>".htmlspecialchars($row['testo'])."</div>";
	echo "<div class='timestamp'>".$row['data']."</div>";
	echo "<a href='#comment_form' class='reply' id='".$row['idCommento']."'>Rispondi</a>";
	/* The following sql checks whether there's any reply for the comment */
	$stmt = $conn->prepare("SELECT * FROM Commento WHERE idCommento_parent = ?");
	$stmt->bindParam(1, $row['idCommento'], PDO::PARAM_STR, 20);
	$stmt->execute();
	if($stmt->rowCount() > 0){
		$commenti = $stmt->fetchAll(PDO::FETCH_ASSOC);
		echo "<ul class='comment-tree'>";
		foreach($commenti as $row)
			getComments($conn, $row);
		echo "</ul>";
	}
	echo "</li>";
}

function update_match_scores($conn, $torneo, $gara, $giocatori, $risultati, $punteggi) {
	if(count($giocatori) != count($risultati) || count($giocatori) != count($punteggi))
		return false;
	$conn->beginTransaction();
	try {
		$stmt = $conn->prepare('UPDATE Partecipa_a '. 
				'SET risultato=? , punteggio=? '.
				'WHERE idTorneo=? AND idGara=? AND '.
				'idGiocatore IN (SELECT idUtente '.
				'FROM Utente WHERE user=?)'
			);

		for($i=0; $i < count($giocatori); $i++) {
			$stmt->bindParam(1, $risultati[$i], PDO::PARAM_STR, 20);
			$stmt->bindParam(2, $punteggi[$i], PDO::PARAM_STR, 20);
			$stmt->bindParam(3, $torneo, PDO::PARAM_STR, 20);
			$stmt->bindParam(4, $gara, PDO::PARAM_STR, 20);
			$stmt->bindParam(5, $giocatori[$i], PDO::PARAM_STR, 20);
			
			$stmt->execute();
		}
		$stmt->closeCursor();
	} catch (Exception $e) {
		$conn->rollBack();
		return false;
	}
	$conn->commit();
	return true;
}


function modify_torneo($conn, $user, $torneo, $nome_torneo, $scadenza, $quota, $premio, $riedizione) {
		$stmt = $conn->prepare('UPDATE Torneo '.
				'SET nome=?, scadenza_iscr=STR_TO_DATE(?, \'%Y-%m-%d\'), quota_iscr=?, '.
				'premio=?, idTorneo_precedente=? '.
				'WHERE idTorneo=?'
			);

		$stmt->bindParam(1, $nome_torneo, PDO::PARAM_STR, 45);
		$stmt->bindParam(2, $scadenza, PDO::PARAM_STR, 20);
		$stmt->bindParam(3, $quota, PDO::PARAM_INT);
		$stmt->bindParam(4, $premio, PDO::PARAM_STR, 45);
		$stmt->bindParam(5, $riedizione, PDO::PARAM_STR, 20);
		$stmt->bindParam(6, $torneo, PDO::PARAM_STR, 20);

		$res = $stmt->execute();
		$stmt->closeCursor();

		return $res;
}
?>
