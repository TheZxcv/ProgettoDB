<?php // Login chek
require_once('functions.php');
$logged = null;
$user = null;
//Verifico l'esistenza del cookie e di $logged
if(isset($_SESSION['is_logged'])){
    $logged=$_SESSION['is_logged'];
    $user=$_SESSION['user'];
}
if($logged){
    $_SESSION['is_logged']=$logged;
    $_SESSION['user']=$user;
}
?>
<nav class="uk-navbar">
	<div class="uk-button uk-navbar-toggle" data-uk-offcanvas="{target:'#main-menu'}"></div>
	<div class="uk-navbar-content uk-navbar-center">
<?php
if(isset($_SESSION['user']))
	print('
		<em>User: ' . $_SESSION['user'] . '</em>'
	);
else
	print('
		<em>Non loggato</em>
');
?>
	</div>
</nav>

<div id="main-menu" class="uk-offcanvas">
    <div class="uk-offcanvas-bar">
    	<ul class="uk-nav uk-nav-parent-icon" data-uk-nav>
		<li><a href="index.php" style="color:grey">Pagina principale</a></li>
		<?php
		// Escludi se non trovo l'utente...
		if(isset($_SESSION['user'])){
		$conn = open_connection();
		$user = $_SESSION['user']; // Dovrebbe essere quello giusto
		if(isAdmin($conn, $user)){
		print('
		<li><a href="admin.php" style="color:grey">Pannello di amministrazione</a></li>
		');
		}
		if(canCreateTournaments($conn, $user)){
		print('
		<li><a href="miei_tornei_organizzati.php" style="color:grey">Gestisci i tornei creati</a></li>
		<li><a href="modifica_iscrizioni.php" style="color:grey">Gestisci le iscrizioni ai tornei</a></li>
		<li><a href="crea_torneo.php" style="color:grey">Crea un torneo</a></li>
		');
		}
		print('
		<li><a href="miei_tornei.php" style="color:grey">Tornei personali</a></li>
		<li><a href="tornei_aperti.php" style="color:grey">Tornei disponibili</a></li>
		<li><a href="lista_tornei.php" style="color:grey">Lista completa dei tornei</a></li>
		<li><a href="account.php" style="color:grey">Account personale</a></li>
		<li><a href="account.php?mod=out" style="color:grey">Logout</a></li>
		');
		} else // e fallo loggare
		print('
		<li><a href="account.php" style="color:grey">Login</a></li>
		<li><a href="signup.php" style="color:grey">Registrazione</a></li>
		');
		?>
	</ul>
    </div>
</div>
