<?php // Login chek
ini_set("error reporting", E_ALL);
session_start();
include_once('lib/functions.php');
$conn = open_connection();
$logged = null;
$user = null;
//Verifico l'esistenza del cookie e di $logged
if(isset($_POST['user'])){
    $myuser=trim($_POST['user']);
    $mypsw=trim($_POST['psw']);
    $logged=check_login($conn, $myuser, $mypsw);
    $user = $myuser;
}
if(isset($_SESSION['is_logged'])){
    $logged=$_SESSION['is_logged'];
    $user=$_SESSION['user'];
}
if($logged){
    $_SESSION['is_logged']=$logged;
    $_SESSION['user']=$user;
}
if(isset($_POST['mod']) && $_POST['mod'] == 'out'){
    unset($_SESSION['is_logged']);
    unset($_SESSION['user']);
    $logged=null;
    unset($_SESSION['mod']);
}

?>
<!DOCTYPE html>
<html>

<head>
<?php include_once('lib/header.php');?>
</head>

<body>

<?php
ini_set("error reporting", E_ALL);
include('lib/menu.php');
unset($conn);
?>

<h2>
Benvenuti nella applicazione web "WebApp Tornei", sviluppata da:
<br>
Nicola Pirritano Giampietro (Matricola 828015)
<br>
Giovanni Santini (Matricola 840332)
<br><br>
Per accedere alle funzionalità disponibili, utilizzare il bottone menù in alto a sinistra.
</h2>

</body>

</html>
