<?php // Login chek
ini_set("error reporting", E_ALL);
session_start();
include_once('lib/functions.php');
$conn = open_connection();
$logged = null;
$user = null;
//Verifico l'esistenza del cookie e di $logged
if(isset($_POST['user'])){
    $myuser=trim($_POST['user']);
    $mypsw=trim($_POST['psw']);
    $logged=check_login($conn, $myuser, $mypsw);
    $user = $myuser;
}
if(isset($_SESSION['is_logged'])){
    $logged=$_SESSION['is_logged'];
    $user=$_SESSION['user'];
}
if($logged){
    $_SESSION['is_logged']=$logged;
    $_SESSION['user']=$user;
}

if(!canCreateTournaments($conn, $user)) {
	header('HTTP/1.0 403 Forbidden');
	header('Location: /redirect.php');
	die();
}

if(isset($_POST['torneo']) && !isset($_GET['torneo']))
	$_GET['torneo'] = $_POST['torneo'];

if(isset($_POST['gioc']) && isset($_POST['data']))
	$result = create_match($conn, $_POST['torneo'], $_POST['data'], $_POST['gioc']);
?>
<!DOCTYPE html>
<html>

<head>
<?php include_once('lib/header.php');?>
	<link href="uikit/css/components/datepicker.css" rel="stylesheet" type="text/css">
	<script src="uikit/js/components/datepicker.min.js"></script>
</head>

<body>
<?php
include('lib/menu.php');
if(isset($result)) {
	if($result == true)
		print('<div class="uk-alert uk-alert-success">Gara creata con successo</div>');
	else
		print('<div class="uk-alert uk-alert-danger">Errore nella creazione della gara</div>');
} else
	print('<div class="uk-alert uk-alert-warning">Dati mancanti per la creazione della gara</div>');

unset($_POST);
?>

<form class="uk-form uk-form-horizontal" method="POST" action="<?php echo($_SERVER['PHP_SELF']);?>">
    <fieldset data-uk-margin>
        <legend>Inserire i dati per creare la gara</legend>

	<div class="uk-form-row">
		<input type="hidden" name="torneo" value="<?php echo($_GET['torneo']) ?>">
		<label class="uk-form-label">Data gara</label>
		<div class="uk-form-controls">
		<input type="text" data-uk-datepicker="{format:'YYYY-MM-DD'}" name="data" placeholder="Data della gara">
		</div>
        </div>

	<div class="uk-form-row">
		<label class="uk-form-label">Giocatori</label>
		<div class="uk-form-controls">
<?php
	$code_to_print = '<select name="gioc[]"><option>Scegli giocatore</option>';
	$stmt=$conn->prepare('SELECT DISTINCT I.idGiocatore, U.user ' . 
			'FROM Utente AS U JOIN Iscritto_a AS I ON U.idUtente = I.idGiocatore ' .
			'WHERE I.idTorneo = ? AND I.approvata = true');
	$stmt->bindParam(1, $_GET['torneo'], PDO::PARAM_STR, 20);
	$stmt->execute();
	$partecipanti = $stmt->fetchAll(PDO::FETCH_ASSOC);
	foreach($partecipanti as $row)
		 $code_to_print .= '<option value="' . $row['idGiocatore'] . '">' . $row['user'] . '</option>';
	$code_to_print .= '</select>';
	$stmt->closeCursor();

	$stmt=$conn->prepare('SELECT max_partecipanti ' .
			'FROM Torneo ' .
			'WHERE idTorneo = ?');
	$stmt->bindParam(1, $_GET['torneo'], PDO::PARAM_STR, 20);
	$stmt->execute();
	$max_part = $stmt->fetchAll(PDO::FETCH_NUM)[0][0];
	$stmt->closeCursor();
	for($i = 0; $i < $max_part; $i++)
		print($code_to_print);

	unset($conn);
?>
		</div>
        </div>
        <input type="hidden" name="torneo" value="<?php echo($_GET['torneo'])?>"/>
	<button class="uk-button">Crea gara</button>
	</fieldset>
</form>

</body>

</html>
